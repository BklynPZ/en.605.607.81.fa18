#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       runner_CDM.py
#
#   PURPOSE:    Provides TestSuite capability.
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import sys
import unittest
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.Tests.Units.CDM.test_CardDispenserMgr import TestCardDispenserMgr
from cluejhu.Tests.Units.CDM.test_CreateCards import TestCreateCards

#---------------------------------------------------------------------------

# initialize the test suite
loader = unittest.TestLoader()
suite = unittest.TestSuite()

# add tests to the test suite
suite.addTests( loader.loadTestsFromModule( TestCardDispenserMgr ) )
suite.addTests( loader.loadTestsFromModule( TestCreateCards ) )

# initialize a runner, pass it your suite and run it
runner = unittest.TextTestRunner( verbosity = 3 )
result = runner.run( suite )