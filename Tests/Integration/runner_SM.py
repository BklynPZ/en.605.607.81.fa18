#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       runner_SM.py
#
#   PURPOSE:    Provides TestSuite capability.
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import sys
import unittest
#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )
from cluejhu.SM import Gamer
#---------------------------------------------------------------------------

# initialize the test suite
loader = unittest.TestLoader()
suite = unittest.TestSuite()

# add tests to the test suite
suite.addTests( loader.loadTestsFromModule( Gamer ) )

# initialize a runner, pass it your suite and run it
runner = unittest.TextTestRunner( verbosity = 3 )
result = runner.run( suite )