#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       runner_HCI.py
#
#   PURPOSE:    Provides TestSuite capability.
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import sys
import unittest

#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )
from cluejhu.Tests.Units.HCI.test_GameBoard import TestGameBoard
from cluejhu.Tests.Units.HCI.test_ProcessRegistration import TestProcessRegistration

#---------------------------------------------------------------------------

# initialize the test suite
loader = unittest.TestLoader()
suite = unittest.TestSuite()

# add tests to the test suite
suite.addTests( loader.loadTestsFromModule( TestGameBoard ) )
suite.addTests( loader.loadTestsFromModule( TestProcessRegistration ) )

# initialize a runner, pass it your suite and run it
runner = unittest.TextTestRunner( verbosity = 3 )
result = runner.run( suite )