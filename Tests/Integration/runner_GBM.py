#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       runner_GBM.py
#
#   PURPOSE:    Provides TestSuite capability.
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import sys
import unittest
#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )
from cluejhu.Tests.Units.GBM.test_dbInterface import TestdBInterface
from cluejhu.Tests.Units.GBM.test_Game import TestGame
from cluejhu.Tests.Units.GBM.test_MultiCastLogger import TestMultiCastLogger
from cluejhu.Tests.Units.GBM.test_NextMoveCalculator import TestNextMoveCalculator
#---------------------------------------------------------------------------

# initialize the test suite
loader = unittest.TestLoader()
suite = unittest.TestSuite()

# add tests to the test suite
suite.addTests( loader.loadTestsFromModule( TestdBInterface ) )
suite.addTests( loader.loadTestsFromModule( TestGame ) )
suite.addTests( loader.loadTestsFromModule( TestMultiCastLogger ) )
suite.addTests( loader.loadTestsFromModule( TestNextMoveCalculator ) )

# initialize a runner, pass it your suite and run it
runner = unittest.TextTestRunner( verbosity = 3 )
result = runner.run( suite )