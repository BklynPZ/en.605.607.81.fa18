#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       test_ProcessRegistration.py
#
#   PURPOSE:    Provides unit testing for the ClueJHU ProcessRegistration class.
#
#   HISTORY:
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import os
import glob
import time
import sys
import unittest
import requests

#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.GBM.Game import Game
from cluejhu.SM.Gamer import Gamer
from cluejhu.GBM.Modules.dB_Interface import SQLInterface
from cluejhu.HCI.HTML.cgi_bin.ProcessRegistration import ProcessRegistration

#---------------------------------------------------------------------------

class TestProcessRegistration( unittest.TestCase ):

    __uut = ProcessRegistration()  # unit under test
    __ust_1 = Game()  # unit supporting test
    __ust_2 = Gamer()  # unit supporting test
    __query = SQLInterface()  # unit supporting test

    __dir = os.path.normpath( '/usr/local/source_code/cluejhu/GBM/logs/system' )

    def setUp( self ):
        """
        -----------------------------------------------------------------------
        Method:  setUp( )::private
        Purpose: sets initial conditions for test environment
        -----------------------------------------------------------------------
        """
        time.sleep( .5 )

        # delete system logs which tends to lock up test execution
        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.log' ) )

            for __file in __files:
                os.unlink( __file )

        self.__ust_1.restartGame()

    def tearDown( self ):
        """
        -----------------------------------------------------------------------
        Method:  tearDown( )::private
        Purpose: resets conditions for next test
        -----------------------------------------------------------------------
        """
        pass

    def testGetFormData( self ):
        """
        -----------------------------------------------------------------------
        Method:  testGetFormData( )::private
        Purpose: spoofs the $_POST method to stimulate code response
        -----------------------------------------------------------------------
        """
        __url = 'http://game.cluejhu.com/cgi_bin/ProcessRegistration.py'
        __form_data = {'name':'player', 'value':'dr_orchid'}
        __session = requests.session()
        __post = requests.get( __url, data = __form_data, verify = False ).text

        __result = __post.find( 'Internal Server Error' )
        self.assertEquals( __result, 0 )

    def testProcessPlayerAssignment_1( self ):
        """
        -----------------------------------------------------------------------
        Method:  testProcessPlayerAssignment( )::private
        Purpose: tests to see if the html returns: CHARACTER ALREADY TAKEN
        -----------------------------------------------------------------------
        """
        __char_name = 'col_mustard'

        self.__ust_2.setGamerStatus( __char_name, True )
        self.__uut.processPlayerAssignment( __char_name )

        __html_code = self.__uut.getHtmlPageResults()
        __result = __html_code.find( 'is already taken' )

        self.assertGreater( __result, 0 )

    def testProcessPlayerAssignment_2( self ):
        """
        -----------------------------------------------------------------------
        Method:  testProcessPlayerAssignment( )::private
        Purpose: tests to see if the html returns: CHARACTER AVAILABLE
        -----------------------------------------------------------------------
        """
        __char_name = 'col_mustard'

        self.__uut.processPlayerAssignment( __char_name )

        __html_code = self.__uut.getHtmlPageResults()
        __result = __html_code.find( 'Congratulations!!!' )

        self.assertEquals( __result, 243 )

    def testProcessPlayerAssignment_3( self ):
        """
        -----------------------------------------------------------------------
        Method:  testProcessPlayerAssignment( )::private
        Purpose: tests to see if the html returns: MAX CHARACTER LOAD REACHED
        -----------------------------------------------------------------------
        """
        __characters = ( 'col_mustard', 'dr_orchid', 'ms_peacock', 'ms_scarlett', 'prof_plum', 'rev_greem' )

        for __char_name in __characters:
            self.__ust_2.setGamerStatus( __char_name, True )

        __char_name = 'ms_scarlett'

        self.__uut.processPlayerAssignment( __char_name )

        __html_code = self.__uut.getHtmlPageResults()
        __result = __html_code.find( 'Maximum' )

        self.assertNotEqual( __result, 0 )

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
if __name__ == "__main__":
    unittest.main()