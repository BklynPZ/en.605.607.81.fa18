#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       test_CreateCards.py
#
#   PURPOSE:    To test card creating capability of the class.
#
#   HISTORY:
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import os
import glob
import time
import sys
import unittest
#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )
from cluejhu.GBM.Modules.dB_Interface import SQLInterface
from cluejhu.CDM.CardDispenserMgr import CardDispenserManager
#---------------------------------------------------------------------------

class TestCreateCards( unittest.TestCase ):

    __uut = CardDispenserManager()
    __dir = os.path.normpath( '/usr/local/source_code/cluejhu/GBM/logs/system' )

    def setUp( self ):
        """
            Setup  wipes out all row-data
        """
        time.sleep( .5 )

        # delete system logs which tends to lock up test execution
        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.log' ) )

            for __file in __files:
                os.unlink( __file )

        __cmd = "DELETE FROM cluejhu_gameboard.tbl_case_file "
        __cmd += "WHERE id > %d;"
        __params = 0

        self.__query = SQLInterface()
        self.__query.SQLExecuteCmd( __cmd, ( __params, ) )

    def tearDown( self ):
        """
            Teardown wipes out all row data
        """
        __cmd = "DELETE FROM cluejhu_gameboard.tbl_case_file "
        __cmd += "WHERE id > %d;"
        __params = 0

        self.__query = SQLInterface()
        self.__query.SQLExecuteCmd( __cmd, ( __params, ) )

    def testCreateGameCard( self ):
        """
            Create game-card which should populate the card_1, card_2 and card_3
            columns with a random person, weapon and room name.
        """
        __x = CardDispenserManager()
        del __x

        __cmd = "SELECT count(id) FROM cluejhu_gameboard.tbl_case_file "
        __cmd += "WHERE card_1 IS NOT %s;"
        __params = "NULL"
        __results = self.__query.SQLExecuteCmd( __cmd, ( __params, ) )

        self.assertEquals( int( __results[0] ), 1 )

    def testCreateGameCards( self ):
        """
            Create game-card which should populate the card_1, card_2 and card_3
            columns with a random person, weapon and room name.
        """
        __a = CardDispenserManager()
        __b = CardDispenserManager()
        __c = CardDispenserManager()
        __d = CardDispenserManager()
        __e = CardDispenserManager()

        del __a, __b, __c, __d, __e

        __cmd = "SELECT count(id) FROM cluejhu_gameboard.tbl_case_file "
        __cmd += "WHERE card_1 IS NOT %s;"
        __params = "NULL"
        __results = self.__query.SQLExecuteCmd( __cmd, ( __params, ) )

        self.assertEquals( int( __results[0] ), 5 )

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

if __name__ == "__main__":
    unittest.main()