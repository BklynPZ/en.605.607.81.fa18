#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       test_CardDispenserMgr.py
#
#   PURPOSE:    Tests the card to ensure game-cards are populated randomly.
#
#   HISTORY:
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import os
import glob
import time
import sys
import unittest

#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )
from cluejhu.GBM.Modules.dB_Interface import SQLInterface
from cluejhu.CDM.CardDispenserMgr import CardDispenserManager
from cluejhu.GBM.Game import Game

#---------------------------------------------------------------------------

class TestCardDispenserMgr( unittest.TestCase ):

    __query = SQLInterface()
    __ust = Game()  # unit supporting test
    __uut = CardDispenserManager()  # unit under test

    __dir = os.path.normpath( '/usr/local/source_code/cluejhu/GBM/logs/system' )

    def setUp( self ):
        """
        -----------------------------------------------------------------------
        Method:  setUp( )::private
        Purpose: sets initial conditions for test environment
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        time.sleep( .5 )

        # delete system logs which tends to lock up test execution
        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.log' ) )

            for __file in __files:
                os.unlink( __file )

        self.__ust.resetCaseFileTbl()

    def tearDown( self ):
        """
        -----------------------------------------------------------------------
        Method:  tearDown( )::private
        Purpose: resets conditions for next test or game execution
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        self.__ust.resetCaseFileTbl()
        self.__uut.resetCardDecks()

        self.__ust.__del__()
        self.__query.__del__()
        self.__uut.__del__()

    def testCreateGameCard( self ):
        """
        -----------------------------------------------------------------------
        Method:  testCreateGameCard( )::private
        Purpose: Create game-card which should populate the card_1, card_2 and card_3
                    columns with a random person, weapon and room name.
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        __uut = CardDispenserManager()  # create a new set of cards
        del __uut

        __cmd = "SELECT count(%s) FROM cluejhu_gameboard.tbl_case_file; "
        __params = "id"
        __results = self.__query.SQLExecuteCmd( __cmd, ( __params, ) )[0]

        self.assertEquals( __results, 2 )

    def testCreateGameCards( self ):
        """
        -----------------------------------------------------------------------
        Method:  testCreateGameCards( )::private
        Purpose: Create multiple game-cards which populates the card_1, card_2 and card_3
                    columns with a random person, weapon and room name.
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        # create random combination of cards

        __a = CardDispenserManager()
        __b = CardDispenserManager()
        __c = CardDispenserManager()
        __d = CardDispenserManager()
        __e = CardDispenserManager()

        del __a, __b, __c, __d, __e

        __cmd = "SELECT count(id) FROM cluejhu_gameboard.tbl_case_file "
        __cmd += "WHERE card_1 IS %s;"
        __params = "NULL"
        __results = self.__query.SQLExecuteCmd( __cmd, ( __params, ) )

        self.assertEquals( int( __results[0] ), 0 )

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

if __name__ == "__main__":
    unittest.main()