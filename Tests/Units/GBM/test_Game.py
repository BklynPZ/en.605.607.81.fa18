#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       test_Game
#
#   PURPOSE:    Provides unit testing for the ClueJHU Game class.
#
#   HISTORY:
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import os
import glob
import time
import sys
import unittest
#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.GBM.Game import Game
from cluejhu.GBM.Modules.dB_Interface import SQLInterface
#---------------------------------------------------------------------------

class TestGame( unittest.TestCase ):

    __query = SQLInterface()
    __uut = Game()

    __dir = os.path.normpath( '/usr/local/source_code/cluejhu/GBM/logs/system' )

    def setUp( self ):
        """
        -----------------------------------------------------------------------
        Method:  setUp( )::private
        Purpose: sets initial conditions for test environment
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        time.sleep( .5 )

        # delete system logs which tends to lock up test execution
        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.log' ) )

            for __file in __files:
                os.unlink( __file )

        self.__uut.restartGame()

    def tearDown( self ):
        """
        -----------------------------------------------------------------------
        Method:  tearDown( )::private
        Purpose: resets conditions for next test
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        self.__uut.restartGame()

    def testResetCaseFileTbl( self ):
        """
        -----------------------------------------------------------------------
        Method:  testResetCaseFileTbl( )::private
        Purpose: tests to ensure the card deck is empty
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        __cmd = "SELECT count(%s) FROM cluejhu_gameboard.tbl_case_file "
        __params = "card_1"
        __result = self.__query.SQLExecuteCmd( __cmd, ( __params ) )

        __cmd = "SELECT count(%s) FROM cluejhu_gameboard.tbl_case_file "
        __params = "card_2"
        __result += self.__query.SQLExecuteCmd( __cmd, ( __params ) )

        __cmd = "SELECT count(%s) FROM cluejhu_gameboard.tbl_case_file "
        __params = "card_3"
        __result += self.__query.SQLExecuteCmd( __cmd, ( __params ) )

        self.assertEquals( int( __result[0] ), 0 )

    def testResetHallTbl( self ):
        """
        -----------------------------------------------------------------------
        Method:  testResetHallTbl( )::private
        Purpose: tests to ensure hall occupancy is empty
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        __result = [0]
        __cmd = "SELECT count(%s) FROM cluejhu_gameboard.tbl_hall;"
        __params = "id"
        __max_range = self.__query.SQLExecuteCmd( __cmd, ( __params, ) )
        __row_count = __max_range[0]

        for __x in range( 1, ( __row_count + 1 ) ):  # loop through all rows
            __cmd = "SELECT count(%s) FROM cluejhu_gameboard.tbl_hall "
            __cmd += "WHERE id = %d and %s IS %s;"
            __params = ( 'is_occupied', __x, 'is_occupied', 'NULL' )
            __result += self.__query.SQLExecuteCmd( __cmd, ( __params ) )
            del __params

        self.assertEquals( sum( __result ), 0 )

    def testResetNextMoveTbl( self ):
        """
        -----------------------------------------------------------------------
        Method:  testResetNextMoveTbl( )::private
        Purpose: tests to ensure NexMove table has empty positions
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        __result = [0]

        __cmd = "SELECT count(%s) FROM cluejhu_gameboard.tbl_next_move;"
        __params = "id"
        __max_range = self.__query.SQLExecuteCmd( __cmd, ( __params, ) )
        __row_count = __max_range[0]

        for __x in range( 1, ( __row_count + 1 ) ):  # loop through all rows
            __cmd = "SELECT count(%s) FROM cluejhu_gameboard.tbl_next_move "
            __cmd += "WHERE id = %d and %s IS %s;"
            __params = ( 'pos_1', __x, 'pos_1', 'NULL' )

            __result += self.__query.SQLExecuteCmd( __cmd, ( __params ) )
            del __params

        for __x in range( 1, ( __row_count + 1 ) ):  # loop through all rows
            __cmd = "SELECT count(%s) FROM cluejhu_gameboard.tbl_next_move "
            __cmd += "WHERE id = %d and %s IS %s;"
            __params = ( 'pos_2', __x, 'pos_2', 'NULL' )

            __result += self.__query.SQLExecuteCmd( __cmd, ( __params ) )
            del __params

        for __x in range( 1, ( __row_count + 1 ) ):  # loop through all rows
            __cmd = "SELECT count(%s) FROM cluejhu_gameboard.tbl_next_move "
            __cmd += "WHERE id = %d and %s IS %s;"
            __params = ( 'pos_3', __x, 'pos_3', 'NULL' )

            __result += self.__query.SQLExecuteCmd( __cmd, ( __params ) )
            del __params

        self.assertEquals( sum( __result ), 0 )

    def testResetNextTurnTbl( self ):
        """
        -----------------------------------------------------------------------
        Method:  testResetNextTurnTbl( )::private
        Purpose: tests to ensure the next turn value is empty
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        __cmd = "SELECT %s FROM %s "
        __params = ( 'id', 'cluejhu_gameboard.tbl_next_turn' )

        __result = self.__query.SQLExecuteCmd( __cmd, __params )[0]

        self.assertEquals( __result, 1 )

    def testResetPlayerSuspectTbl( self ):
        """
        -----------------------------------------------------------------------
        Method:  testResetPlayerSuspectTbl( )::private
        Purpose: tests to ensure tbl_player_suspect's active and suspect values are empty
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        __characters = ['col_mustard', 'dr_orchid', 'ms_peacock', 'ms_scarlett', 'prof_plum', 'rev_green']
        __result = [0]

        for __character in __characters:
            __cmd = "SELECT sum(%s + %s) AS total FROM cluejhu_gameboard.tbl_player_suspect "
            __cmd += "WHERE name='%s';"
            __params = ( 'is_active', 'is_suspect', __character )

            __result += self.__query.SQLExecuteCmd( __cmd, ( __params ) )
            del __params

        self.assertEquals( sum( __result ), 0 )

    def testResetGamePlayLogs( self ):
        """
        -----------------------------------------------------------------------
        Method:  testResetGamePlayLogs( )::private
        Purpose: tests to ensure gameplay logs are deleted
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        self.__uut.resetGamePlayLogs()

        __dir = "/usr/local/source_code/cluejhu/GBM/logs/gameplay"

        __path, __dirs, __files = next( os.walk( __dir ) )
        __file_count = len( __files )

        self.assertEqual( __file_count, 0 )
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

if __name__ == "__main__":
    unittest.main()