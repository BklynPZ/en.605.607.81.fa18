#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       test_dbInterface
#
#   PURPOSE:    Provides unit testing for the ClueJHU NextMoveCalculator class.
#
#   HISTORY:
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import os
import glob
import time
import sys
import unittest
#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.SM.Gamer import Gamer
from cluejhu.GBM.NextMoveCalculator import NextMoveCalculator
#---------------------------------------------------------------------------

class TestNextMoveCalculator( unittest.TestCase ):

    __uut = NextMoveCalculator()  # unit under test
    __ust = Gamer()  # unit supporting test

    __dir = os.path.normpath( '/usr/local/source_code/cluejhu/GBM/logs/system' )

    def setUp( self ):
        """
        -----------------------------------------------------------------------
        Method:  setUp( )::private
        Purpose: sets initial conditions for test environment
        -----------------------------------------------------------------------
        """
        time.sleep( .5 )

        # delete system logs which tends to lock up test execution
        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.log' ) )

            for __file in __files:
                os.unlink( __file )

        self.__ust.resetPlayerSuspectTbl()

    def tearDown( self ):
        """
        -----------------------------------------------------------------------
        Method:  tearDown( )::private
        Purpose: resets conditions for next test
        -----------------------------------------------------------------------
        """
        pass

if __name__ == "__main__":
    unittest.main()