#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       test_dbInterface
#
#   PURPOSE:    Provides unit testing for the ClueJHU MultiCaseLogger class.
#
#   HISTORY:
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import os
import glob
import time
import sys
import socket
import unittest
#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )
from cluejhu.SM.Gamer import Gamer
from cluejhu.GBM.Modules.MultiCastLogger import MultiCastLogger
#---------------------------------------------------------------------------

class TestMultiCastLogger( unittest.TestCase ):

    __uut = MultiCastLogger()  # unit under test
    __ust = Gamer()  # unit supporting test

    __dir_g = os.path.normpath( '/usr/local/source_code/cluejhu/GBM/logs/gameplay' )
    __dir_s = os.path.normpath( '/usr/local/source_code/cluejhu/GBM/logs/system' )

    def setUp( self ):
        """
        -----------------------------------------------------------------------
        Method:  setUp( )::private
        Purpose: sets initial conditions for test environment
        -----------------------------------------------------------------------
        """
        time.sleep( .5 )

        # delete system logs which tends to lock up test execution
        if os.path.isdir( self.__dir_s ):
            __files = glob.glob( os.path.join( self.__dir_s, '*.log' ) )
            for __file in __files:
                os.unlink( __file )

        # delete multicast logs which tends to lock up test execution
        if os.path.isdir( self.__dir_g ):
            __files = glob.glob( os.path.join( self.__dir_g, '*.txt' ) )
            for __file in __files:
                os.remove( __file )

    def tearDown( self ):
        """
        -----------------------------------------------------------------------
        Method:  tearDown( )::private
        Purpose: resets conditions for next test
        -----------------------------------------------------------------------
        """
        pass

    def testCheckEmptyMultiCastDir( self ):
        """
        -----------------------------------------------------------------------
        Method:  testCheckEmptyMultiCastDir( )::private
        Purpose: tests a class's ability to remove pre-existing multicast logs
        -----------------------------------------------------------------------
        """
        __success = False

        if os.path.isdir( self.__dir_g ):
            __files = glob.glob( os.path.join( self.__dir_g, '*.txt' ) )

        if __files.__len__() == 0:
            __success = True

        self.assertTrue( __success )

    def testGetIpAddress( self ):
        """
        -----------------------------------------------------------------------
        Method:  testGetIpAddress( )::private
        Purpose: tests a method's ability to multicast get the correct IP address
        -----------------------------------------------------------------------
        """
        __ip = self.__uut.getIpAddress()

        __s = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        __s.connect( ( "8.8.8.8", 80 ) )
        __expected_ip = ( __s.getsockname()[0] )
        __s.close()

        self.assertEqual( __ip, __expected_ip )

    def testLogPlayerMove( self ):
        """
        -----------------------------------------------------------------------
        Method:  testLogPlayerMove( )::private
        Purpose: tests a method's ability to multicast a player moved
        -----------------------------------------------------------------------
        """
        __success = False
        __file = os.path.join( self.__dir_g, 'multicast-messages.txt' )
        __gamer = 'mr_integration'
        __location = '1_1H1_2'

        self.__uut.logPlayerMove( __gamer, __location )

        with open( __file, 'r' ) as __filename:
            __first_line = __filename.readline()

        if __first_line.find( 'Mr. Integration' ) and __first_line.find( 'East Study Hallway' ):
            __success = True

        self.assertEquals( __success, True )

    def testLogSuggestion( self ):
        """
        -----------------------------------------------------------------------
        Method:  testLogSuggestion( )::private
        Purpose: tests a method's ability to multicast a new suggestion was made
        -----------------------------------------------------------------------
        """
        __success = False

        __player = 'mr_integration'
        __suspect = 'mr_slimShady'
        __weapon = 'knife'
        __room = 'kitchen'
        __file = os.path.join( self.__dir_g, 'multicast-messages.txt' )

        self.__uut.logSuggestion( __player, __suspect, __weapon, __room )

        with open( __file, 'r' ) as __filename:
            __first_line = __filename.readline()

        if __first_line.find( 'Mr. Integration' ) and __first_line.find( 'Mr. SlimShady' ) and __first_line.find( 'Knife' ) and __first_line.find( 'Kitchen' ):
            __success = True

        self.assertEquals( __success, True )

    def testLogAccusation( self ):
        """
        -----------------------------------------------------------------------
        Method:  testLogAccusation( )::private
        Purpose: tests a method's ability to multicast a new accusation was made
        -----------------------------------------------------------------------
        """
        __success = False

        __player = 'mr_integration'
        __suspect = 'ms_scarlett'
        __weapon = 'knife'
        __room = 'kitchen'
        __file = os.path.join( self.__dir_g, 'multicast-messages.txt' )

        self.__uut.logAccusation( __player, __suspect, __weapon, __room )

        with open( __file, 'r' ) as __filename:
            __first_line = __filename.readline()

        if __first_line.find( 'Mr. Integration' ) and __first_line.find( 'Mr. Scarlett' ) and __first_line.find( 'Knife' ) and __first_line.find( 'Kitchen' ):
            __success = True

        self.assertEquals( __success, True )

    def testLogNewPlayer( self ):
        """
        -----------------------------------------------------------------------
        Method:  testLogNewPlayer( )::private
        Purpose: tests method's ability to log a new user notification
        -----------------------------------------------------------------------
        """
        __success = False
        __player = 'mr_integration'
        __file = os.path.join( self.__dir_g, 'multicast-messages.txt' )

        self.__uut.logNewPlayer( __player )

        with open( __file, 'r' ) as __filename:
            __first_line = __filename.readline()

        if __first_line.find( 'Mr. Integration' ) and __first_line.find( 'joined the game' ):
            __success = True

        self.assertEquals( __success, True )

    def testLogGamerStatus( self ):
        """
        -----------------------------------------------------------------------
        Method:  testLogGamerStatus( )::private
        Purpose: tests the ability to update gamer status (True/False)
        -----------------------------------------------------------------------
        """
        __success = False
        __status = True
        __player = 'col_mustard'
        __reason = 'upset stomach.'

        __file = os.path.join( self.__dir_g, 'multicast-messages.txt' )
        self.__uut.logStatus( __player, __status, __reason )

        __status = self.__ust.getGamerStatus( __player )[0]

        with open( __file, 'r' ) as __filename:
            __first_line = __filename.readline()

        if __first_line.find( 'Col. Mustard' ) and bool( __status ) == True:
            __success = True

        self.assertEquals( __success, True )

    def testMultipleInputs( self ):
        """
        -----------------------------------------------------------------------
        Method:  testMultipleInputs( )::private
        Purpose: tests the ability to write multiple items to multi-cast log file
        -----------------------------------------------------------------------
        """
        __file = os.path.join( self.__dir_g, 'multicast-messages.txt' )

        self.__uut.logNewPlayer( 'col_mustard' )
        self.__uut.logNewPlayer( 'dr_orchid' )
        self.__uut.logNewPlayer( 'ms_scarlett' )

        with open( __file, 'r' ) as __filename:
            __lines = __filename.readlines()

        __lines = [__x.strip() for __x in __lines]

        self.assertEqual( __lines.__len__(), 3 )

if __name__ == "__main__":
    unittest.main()