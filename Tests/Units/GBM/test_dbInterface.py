#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       test_dbInterface
#
#   PURPOSE:    Provides unit testing for the ClueJHU db_Interface class.
#
#   HISTORY:
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import os
import glob
import time
import sys
import unittest

#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.GBM.Modules.dB_Interface import SQLInterface

#---------------------------------------------------------------------------

class TestdBInterface( unittest.TestCase ):

    __uut = SQLInterface()

    __dir = os.path.normpath( '/usr/local/source_code/cluejhu/GBM/logs/system' )

    def setUp( self ):
        """
        -----------------------------------------------------------------------
        Method:  setUp( )::private
        Purpose: sets initial conditions for test environment
        -----------------------------------------------------------------------
        """
        time.sleep( .5 )

        # delete system logs which tends to lock up test execution
        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.log' ) )

            for __file in __files:
                os.unlink( __file )

    def tearDown( self ):
        """
        -----------------------------------------------------------------------
        Method:  tearDown( )::private
        Purpose: resets conditions for next test
        -----------------------------------------------------------------------
        """
        pass

    def testTestConnection( self ):
        """
        -----------------------------------------------------------------------
        Method:  tearDown( )::private
        Purpose: resets conditions for next test
        -----------------------------------------------------------------------
        """
        __result = self.__uut.testConnection()

        self.assertTrue( __result )

if __name__ == "__main__":
    unittest.main()