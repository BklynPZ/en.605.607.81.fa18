#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       test_Logger
#
#   PURPOSE:    Provides unit testing for the ClueJHU Logger class.
#
#   HISTORY:
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import os
import sys
import glob
import time
import unittest

#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.GBM.logs.Logger import Logger

#---------------------------------------------------------------------------
class Test( unittest.TestCase ):

    __uut = Logger()  # unit under test
    __dir = os.path.normpath( '/usr/local/source_code/cluejhu/GBM/logs/system' )

    def setUp( self ):
        """
        -----------------------------------------------------------------------
        Method:  setUp( )::private
        Purpose: sets initial conditions for test environment
        -----------------------------------------------------------------------
        """
        time.sleep( .5 )

        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.log' ) )

            for __file in __files:
                os.unlink( __file )

    def tearDown( self ):
        """
        -----------------------------------------------------------------------
        Method:  tearDown( )::private
        Purpose: resets conditions for next test
        -----------------------------------------------------------------------
        """
        time.sleep( 2 )

        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.log' ) )

            for __file in __files:
                os.unlink( __file )

    def testCheckEmptyLoggerDir( self ):
        """
        -----------------------------------------------------------------------
        Method:  testCheckEmptyLoggerDir( )::private
        Purpose: tests the Logger's ability to delete existing logs
        -----------------------------------------------------------------------
        """
        __success = False

        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.log' ) )

        if __files.__len__() == 0:
            __success = True

        self.assertTrue( __success )

    def testCreateFile( self ):
        """
        -----------------------------------------------------------------------
        Method:  testCreateFile( )::private
        Purpose: tests to see if 1 log file can be successfully created
        -----------------------------------------------------------------------
        """

        __success = False

        __ust = TestClassUnit_1()  # unit supporting test

        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.log' ) )

        if __files.__len__() == 1:
            __success = True
        else:
            __success = False
        del __ust

        self.assertEquals( __success, True )

    def testCreateFiles( self ):
        """
        -----------------------------------------------------------------------
        Method:  testCreateFiles( )::private
        Purpose: tests to see if 3 log file2 can be successfully created
        -----------------------------------------------------------------------
        """
        __success = True

        __ust_1 = TestClassUnit_1()  # unit supporting test
        __ust_2 = TestClassUnit_2()  # unit supporting test
        __ust_3 = TestClassUnit_3()  # unit supporting test

        __files = glob.glob( os.path.join( self.__dir, '*.log' ) )
        __files.sort()

        __log_file_1 = os.path.join( self.__dir, 'Module.TestClassUnit_1.log' )
        __log_file_2 = os.path.join( self.__dir, 'Module.TestClassUnit_2.log' )
        __log_file_3 = os.path.join( self.__dir, 'Module.TestClassUnit_3.log' )

        __set = set( __files )

        if __log_file_1 in __set: __success *= True
        else: __success *= False

        if __log_file_2 in __set: __success *= True
        else: __success *= False

        if __log_file_3 in __set: __success *= True
        else: __success *= False

        del __ust_1, __ust_2, __ust_3

        time.sleep( 1 )

        self.assertEquals( __success, True )

    def testCheckForInfo( self ):
        """
        -----------------------------------------------------------------------
        Method:  testCheckForInfo( )::private
        Purpose: tests for logged input in file
        -----------------------------------------------------------------------
        """
        __success = True

        __ust_1 = TestClassUnit_1()  # unit supporting test
        __ust_1.TCU_INFO()

        __file = os.path.join( self.__dir, 'Module.TestClassUnit_1.log' )

        with open( __file, 'r' ) as __filename:
            __lines = __filename.readlines()

        if not __lines.count( 'information' ) == -1:
            __success *= True
        else:
            __success *= False

        del __ust_1

        self.assertEquals( __success, True )

    def testCheckForWarn( self ):
        """
        -----------------------------------------------------------------------
        Method:  testCheckForWarn( )::private
        Purpose: tests for logged input in file
        -----------------------------------------------------------------------
        """
        __success = True

        __ust_1 = TestClassUnit_1()  # unit supporting test
        __ust_1.TCU_WARN()

        __file = os.path.join( self.__dir, 'Module.TestClassUnit_1.log' )

        with open( __file, 'r' ) as __filename:
            __lines = __filename.readlines()

        if not __lines.count( 'warning' ) == -1:
            __success *= True
        else:
            __success *= False

        del __ust_1

        self.assertEquals( __success, True )

    def testCheckForError( self ):
        """
        -----------------------------------------------------------------------
        Method:  testCheckForError( )::private
        Purpose: tests for logged input in file
        -----------------------------------------------------------------------
        """
        __success = True

        __ust_1 = TestClassUnit_1()  # unit supporting test
        __ust_1.TCU_ERROR()

        __file = os.path.join( self.__dir, 'Module.TestClassUnit_1.log' )

        with open( __file, 'r' ) as __filename:
            __lines = __filename.readlines()

        if not __lines.count( 'error' ) == -1:
            __success *= True
        else:
            __success *= False

        del __ust_1

        self.assertEquals( __success, True )

    def testCheckForCritical( self ):
        """
        -----------------------------------------------------------------------
        Method:  testCheckForCritical( )::private
        Purpose: tests for logged input in file
        -----------------------------------------------------------------------
        """
        __success = True

        __ust_1 = TestClassUnit_1()  # unit supporting test
        __ust_1.TCU_CRITICAL()

        __file = os.path.join( self.__dir, 'Module.TestClassUnit_1.log' )

        with open( __file, 'r' ) as __filename:
            __lines = __filename.readlines()

        if not __lines.count( 'critical' ) == -1:
            __success *= True
        else:
            __success *= False

        del __ust_1

        self.assertEquals( __success, True )

    def testCheckForAll( self ):
        """
        -----------------------------------------------------------------------
        Method:  testCheckForAll( )::private
        Purpose: tests for logged input in file
        -----------------------------------------------------------------------
        """
        __success = True

        __ust_1 = TestClassUnit_1()  # unit supporting test
        __ust_1.TCU_INFO()
        __ust_1.TCU_WARN()
        __ust_1.TCU_ERROR()
        __ust_1.TCU_CRITICAL()

        __file = os.path.join( self.__dir, 'Module.TestClassUnit_1.log' )

        with open( __file, 'r' ) as __filename:
            __lines = __filename.readlines()

        if not __lines.count( 'information' ) == -1: __success *= True
        else: __success *= False

        if not __lines.count( 'critical' ) == -1: __success *= True
        else: __success *= False

        if not __lines.count( 'critical' ) == -1: __success *= True
        else: __success *= False

        if not __lines.count( 'critical' ) == -1: __success *= True
        else: __success *= False

        del __ust_1

        self.assertEquals( __success, True )

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

class TestClassUnit_1:

    def __init__( self ):
        self.log = Logger()
        self.log.msg.info( 'Logger.__init__() is the initiating Unit Under Test (1)' )

    def TCU_INFO( self ):
        self.log.msg.info( 'information' )

    def TCU_WARN( self ):
        self.log.msg.warning( 'warning' )

    def TCU_ERROR( self ):
        self.log.msg.error( 'error' )

    def TCU_CRITICAL( self ):
        self.log.msg.critical( 'critical' )

    def TCU_ALL( self ):
        self.log.msg.info( 'information' )
        self.log.msg.warning( 'warning' )
        self.log.msg.error( 'error' )
        self.log.msg.critical( 'critical' )

    def __del__( self ):
        pass

class TestClassUnit_2:

    def __init__( self ):
        self.log = Logger()
        self.log.msg.info( 'Logger.__init__() is the initiating Unit Under Test (2)' )

    def __del__( self ):
        pass

class TestClassUnit_3:

    def __init__( self ):
        self.log = Logger()
        self.log.msg.info( 'Logger.__init__() is the initiating Unit Under Test (3)' )

    def __del__( self ):
        pass
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

if __name__ == "__main__":
    unittest.main()