#!/usr/bin/python
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#
#   NAME:       test_dbInterface
#
#   PURPOSE:    Provides unit testing for the ClueJHU Game class.
#
#   HISTORY:
#
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
import os
import glob
import time
import sys
import unittest
#---------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.GBM.Game import Game
from cluejhu.SM.Gamer import Gamer
from cluejhu.GBM.Modules.dB_Interface import SQLInterface

#---------------------------------------------------------------------------
class TestGamer( unittest.TestCase ):

    __ust = Game()
    __uut = Gamer()
    __query = SQLInterface()

    __dir = os.path.normpath( '/usr/local/source_code/cluejhu/GBM/logs/system' )

    def setUp( self ):
        """
        -----------------------------------------------------------------------
        Method:  setUp( )::private
        Purpose: sets initial conditions for test environment
        -----------------------------------------------------------------------
        """
        time.sleep( .5 )

        # delete system logs which tends to lock up test execution
        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.log' ) )

            for __file in __files:
                os.unlink( __file )

        self.__ust.resetPlayerSuspectTbl()

    def tearDown( self ):
        """
        -----------------------------------------------------------------------
        Method:  tearDown( )::private
        Purpose: resets conditions for next test
        -----------------------------------------------------------------------
        """
        pass

    def testConvertProperName( self ):
        """
        -----------------------------------------------------------------------
        Method:  testConvertProperName( )::private
        Purpose: tests method to convert game-tag name into a formatted name
        -----------------------------------------------------------------------
        """
        __name = 'mr_patrick'
        __name = self.__uut.convertToProperName( __name )

        self.assertEquals( __name, 'Mr. Patrick' )

    def testSetGamerPositionCurrPos( self ):
        """
        -----------------------------------------------------------------------
        Method:  testSetGamerPositionCurrPos( )::private
        Purpose: tests method the set gamer position based on a passed name and new location
        -----------------------------------------------------------------------
        """
        __name = 'col_mustard'
        __cur_pos = 'new_location'
        self.__uut.setGamerPosition( __name, __cur_pos )

        __cmd = "SELECT %s FROM %s WHERE id=%d"
        __params = ( 'cur_pos', 'cluejhu_gameboard.tbl_player_suspect', 1 )
        __result = self.__query.SQLExecuteCmd( __cmd, __params )

        self.assertEquals( __result[0], __cur_pos )

    def testSetGamerPositionLastPos( self ):
        """
        -----------------------------------------------------------------------
        Method:  testSetGamerPositionLastPos( )::private
        Purpose: tests method to set the last gamer position based on setting the initial position
        -----------------------------------------------------------------------
        """
        __name = 'col_mustard'
        __cur_pos = 'this_location'

        self.__uut.setGamerPosition( __name, __cur_pos )

        __cur_pos = 'this_next_location'

        self.__uut.setGamerPosition( __name, __cur_pos )

        __cmd = "SELECT %s FROM %s WHERE id=%d"
        __params = ( 'last_pos', 'cluejhu_gameboard.tbl_player_suspect', 1 )
        __result = self.__query.SQLExecuteCmd( __cmd, __params )

        self.assertEquals( __result[0], 'this_location' )

    def testSetGamerStatus( self ):
        """
        -----------------------------------------------------------------------
        Method:  testSetGamerStatus( )::private
        Purpose: tests method the return the current gamer status (1|0)
        -----------------------------------------------------------------------
        """
        __name = 'prof_plum'
        self.__uut.setGamerStatus( __name, True )

        __id = self.__uut.getGamerIdByName( 'prof_plum' )
        __cmd = "SELECT %s FROM %s WHERE id=%d"
        __params = ( 'is_active', 'cluejhu_gameboard.tbl_player_suspect', __id )
        __result = self.__query.SQLExecuteCmd( __cmd, __params )

        self.assertEquals( __result[0], True )

    def testGetGamerNameById( self ):
        """
        -----------------------------------------------------------------------
        Method:  testGetGamerNameById( )::private
        Purpose: tests method to return the gamer tag name by ID passed
        -----------------------------------------------------------------------
        """
        __name = 'prof_plum'
        __id = 5
        __result = self.__uut.getGamerNameById( 5 )

        self.assertEqual( __name, __result )

    def testGetGamerIdByName( self ):
        """
        -----------------------------------------------------------------------
        Method:  testGetGamerIdByName( )::private
        Purpose: tests method to return the gamer ID based on tag name passed
        -----------------------------------------------------------------------
        """
        __name = 'prof_plum'
        __id = 5
        __result = self.__uut.getGamerIdByName( __name )

        self.assertEqual( __id, 5 )

    def testGetActiveGamers( self ):
        """
        -----------------------------------------------------------------------
        Method:  testGetActiveGamers( )::private
        Purpose: tests method's ability to return the current number of active users
                 ensuring the number is less than or equal to 6
        -----------------------------------------------------------------------
        """
        __count = self.__uut.getActiveGamers()

        self.assertLessEqual( __count, 6 )

if __name__ == "__main__":
    unittest.main()