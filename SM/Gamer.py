#!/usr/bin/python
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#
#   NAME:      gamer.py
#
#   PURPOSE:   Provides functionality for the Gamer-type character.
#
#   HISTORY:
#                CJ-123 | 2018.01.24 | Restructuring of internal system messages
#                CJ-072 | 2018.01.20 | Logging internal system messages
#                CJ-122 | 2019.01.18 | Code clean-up based on Codacy Peer Review
#                CJ-110 | 2018.11.08 | Code clean-up based on Codacy Peer Review
#                CJ-109 | 2018.11.03 | Import code to source control as baseline
#
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
import sys

#-------------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.GBM.logs.Logger import Logger
from cluejhu.GBM.Modules.dB_Interface import SQLInterface

#-------------------------------------------------------------------------------
class Gamer:

    def __init__( self ):
        """
        -----------------------------------------------------------------------
        Method:  __init__( )::public
        Purpose: provides initialization mechanism for object creation.
        Inputs:  None
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        self.log = Logger()
        self.__query = SQLInterface()
        self.log.msg.info( 'construction of object class object {{' + self.__class__.__name__ + '}} complete.' )

    def __del__( self ):
        """
        -----------------------------------------------------------------------
        Method:  __del__()::public
        Purpose: provides final garbage collection for objects before object destruction.
        Inputs:  None.
        Outputs: None.
        -----------------------------------------------------------------------
        """
        self.log.msg.info( 'destruction of object class object {{' + self.__class__.__name__ + '}} complete.' )

    def convertToProperName( self, __name ):
        """
        -----------------------------------------------------------------------
        Method:  convertToProperName()::private
        Purpose: converts gamer name into a proper human-readable name.
        Inputs:  name of the gamer.
        Outputs: formatted name of the gamer (title + surname).
        -----------------------------------------------------------------------
        """
        __pname = __name.__str__().split( '_' )
        __name = __pname[0].title() + '. ' + __pname[1].title()

        return __name.__str__()

    def setGamerPosition( self, __name, __cur_pos ):
        """
        -----------------------------------------------------------------------
        Method:  setCurrentPos()::public
        Purpose: updates t    </div>he database with the current position of the gamer and 
                 moves the "old" current position to the last position.
        Inputs:  gamer name, position of gamer.
        Outputs: None.
        -----------------------------------------------------------------------
        """
        __id = self.getGamerIdByName( __name )
        __prev_pos = self.getCurrentPos( __id )

        __cmd = "UPDATE cluejhu_gameboard.tbl_player_suspect "
        __cmd += "SET cur_pos='%s', last_pos='%s' "
        __cmd += "WHERE id=%d;"
        __params = ( __cur_pos, __prev_pos, __id )

        try:
            self.__query.SQLExecuteCmd( __cmd, __params )
        except:
            self.log.msg.error( 'unable to process command: ' + __cmd % __params )
        else:
            self.log.msg.info( 'setting gamer position (' + self.convertToProperName( __name ) + ') to ' + str( __cur_pos ) )

    def setGamerStatus( self, __name, __status = False ):
        """
        -----------------------------------------------------------------------
        Method:  setgamerStatus()::public
        Purpose: sets the gamer's suspect status in database.
        Inputs:  name of gamer, current status (0|1).
        Outputs: None.
        -----------------------------------------------------------------------
        """
        __id = self.getGamerIdByName( __name )

        __cmd = 'UPDATE cluejhu_gameboard.tbl_player_suspect '
        __cmd += 'SET is_active=%d '
        __cmd += 'WHERE id=%d;'
        __params = ( __status, int( __id ) )

        try:
            self.__query.SQLExecuteCmd( __cmd, __params )
        except:
            self.log.msg.error( 'unable to process command: ' + __cmd % __params )
        else:
            self.log.msg.info( 'setting gamer (' + self.convertToProperName( __name ) + ') \'s status to ' + str( __status ) )

    def setSuspectStatus( self, __name, __status = False ):
        """
        -----------------------------------------------------------------------
        Method:  setSuspectStatus()::public
        Purpose: sets the gamer's suspect status in database.
        Inputs:  name of gamer, current status (0|1).
        Outputs: None.
        -----------------------------------------------------------------------
        """
        __id = self.getGamerIdByName( __name )

        __cmd = 'UPDATE cluejhu_gameboard.tbl_player_suspect '
        __cmd += 'SET is_suspect=%d '
        __cmd += 'WHERE id=%d;'

        __params = ( __status, __id )

        try:
            self.__query.SQLExecuteCmd( __cmd, __params )
        except:
            self.log.msg.error( 'unable to process command: ' + __cmd % __params )
        else:
            self.log.msg.info( 'setting gamer (' + str( self.convertToProperName( __name ) ) + ') suspect status to ' + str( __status ) )

    def getCurrentPos( self, __id ):
        """
        -----------------------------------------------------------------------
        Method:  getCurrentPos()::public
        Purpose: retrieves the current gamer's position on the gameboard.
        Inputs:  id of the gamer.
        Outputs: position of the gamer on the gameboard.
        -----------------------------------------------------------------------
        """
        __cmd = 'SELECT cur_pos FROM cluejhu_gameboard.tbl_player_suspect '
        __cmd += 'WHERE id=%d;'
        __params = ( __id, )
        __pos = self.__query.SQLExecuteCmd( __cmd, __params )

        return __pos[0]

    def getGamerStatus( self, __name ):
        """
        -----------------------------------------------------------------------
        Method:  getGamerStatus()::public
        Purpose: retrieves the status (0|1) of the gamer's activity and suspect.
        Inputs:  the gamer's name.
        Outputs: a list of the gamer's status [is-active | is-suspect].
        -----------------------------------------------------------------------
        """
        self.log.msg.info( 'gamer status for (' + self.convertToProperName( __name ) + ') has been requested' )

        __id = self.getGamerIdByName( __name )

        __cmd = "SELECT is_active FROM cluejhu_gameboard.tbl_player_suspect "
        __cmd += "WHERE id=%d;"
        __params = ( __id, )

        __is_active = self.__query.SQLExecuteCmd( __cmd, __params )

        __cmd = "SELECT is_suspect FROM cluejhu_gameboard.tbl_player_suspect "
        __cmd += "WHERE id=%d;"
        __params = ( __id, )

        __is_suspect = self.__query.SQLExecuteCmd( __cmd, __params )

        __is_active = bool( __is_active[0] )
        __is_suspect = bool( __is_suspect[0] )

        self.log.msg.info( 'database results show (' + self.convertToProperName( __name ) + ') active status is: ' + str( __is_active ) )
        self.log.msg.info( 'database results show (' + self.convertToProperName( __name ) + ') suspect status is' + str( __is_suspect ) )

        return ( __is_active, __is_suspect )

    def getGamerNameById( self, __id ):
        """
        -----------------------------------------------------------------------
        Method:  getGamerNameById()::public
        Purpose: returns the name of the gamer based on the ID passed.
        Inputs:  id of the gamer.
        Outputs: name of the gamer.
        -----------------------------------------------------------------------
        """
        __cmd = "SELECT name FROM cluejhu_gameboard.tbl_player_suspect "
        __cmd += "WHERE id=%d;"
        __params = ( int( __id ), )

        __name = self.__query.SQLExecuteCmd( __cmd, __params )

        return __name[0]

    def getGamerIdByName( self, __name ):
        """
        -----------------------------------------------------------------------
        Method:  getGamerIdByName()::public
        Purpose: returns the ID of the gamer base on the ID passed.
        Inputs:  name of the gamer.
        Outputs: id of the gamer.
        -----------------------------------------------------------------------
        """
        __cmd = "SELECT id FROM cluejhu_gameboard.tbl_player_suspect "
        __cmd += "WHERE name='%s';"
        __params = ( __name, )

        __id = self.__query.SQLExecuteCmd( __cmd, __params )

        return __id[0]

    def getActiveGamers( self ):
        """
        -----------------------------------------------------------------------
        Method:  getActiveGamers()::public
        Purpose: returns the number of active gamers
        Inputs:  None.
        Outputs: number of active gamers in the database.
        -----------------------------------------------------------------------
        """
        __cmd = "SELECT count(is_active) FROM cluejhu_gameboard.tbl_player_suspect WHERE is_active = %d;"
        __results = self.__query.SQLExecuteCmd( __cmd, ( 1, ) )
        __num_active_gamers = int( __results[0] )

        return __num_active_gamers

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------