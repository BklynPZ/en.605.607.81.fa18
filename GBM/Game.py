#!/usr/bin/python
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#
#   NAME:       Game.py
#
#   PURPOSE:    The
#
#   HISTORY:
#                CJ-101 | 2018.01.27 | Initiate game upon minimal game load
#                CJ-123 | 2018.01.24 | Restructuring of internal system messages
#                CJ-110 | 2018.11.08 | Code clean-up based on Codacy Peer Review
#                CJ-109 | 2018.11.03 | Import code to source control as baseline
#
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
import sys
import os
import glob
import datetime

#-------------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.GBM.logs.Logger import Logger
from cluejhu.GBM.Modules.dB_Interface import SQLInterface

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class Game:

    def __init__( self ):
        """
        ------------------------------------------------------------------------
        Method:  __init__( )::public
        Purpose: provides initialization mechanism during object creation.
        Inputs:  None
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        self.log = Logger()
        self.__query = SQLInterface()
        self.__dir = '/usr/local/source_code/cluejhu/GBM/logs/gameplay'
        self.log.msg.info( 'construction of object class object {{' + self.__class__.__name__ + '}} complete.' )

    def __del__( self ):
        """
        --------------------------------------------------------------------
        Method:  __del__()::public
        Purpose: provides final garbage collection for objects before object destruction.
        Inputs:  None.
        Outputs: None.
        --------------------------------------------------------------------
        """
        self.__query.__del__()
        self.log.msg.info( 'destruction of object class object {{' + self.__class__.__name__ + '}} complete.' )

    def restartGame( self ):
        """
        --------------------------------------------------------------------
        Method:  restartGame( )::public
        Purpose: updates all database tables to initial state
        Inputs:  None.
        Outputs: Pass/Fail (True/False) result of executed methods
        --------------------------------------------------------------------"""
        __results = [ ]
        __results.append( self.resetCaseFileTbl() )
        __results.append( self.resetHallTbl() )
        __results.append( self.resetNextMoveTbl() )
        __results.append( self.resetNextTurnTbl() )
        __results.append( self.resetPlayerSuspectTbl() )
        __results.append( self.resetGamePlayLogs() )
        __results.append( self.resetGameStartTime() )

        __result = reduce( lambda x, y: x * y, __results, 1 )

        return bool( __result )

    def resetGameStartTime( self ):
        """
        -----------------------------------------------------------------------
        Method:  resetGameStartTime( )::protected
        Purpose: updates the (tbl_game_time) to restart the game time
        Inputs:  None.
        Outputs: execution results: True/False
        -----------------------------------------------------------------------
        """
        __success = False
        __date_time = datetime.date( 0001, 01, 01 )

        __cmd = "UPDATE %s "
        __cmd += "SET start_time='%s', is_active=%d "
        __cmd += "WHERE id=%d"
        __params = ( 'cluejhu_gameboard.tbl_game_time', __date_time, 0, 1 )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetGameStartTime()" )
            __success = False
        else:
            self.log.msg.info( "query result was successful in resetGameStartTime()" )
            __success = True

        return __success

    def setStartGameTime( self ):
        """
        -----------------------------------------------------------------------
        Method:  setStartGameTime( )::protected
        Purpose: updates the (tbl_game_time) to start the game time
        Inputs:  None.
        Outputs: execution results: True/False
        -----------------------------------------------------------------------
        """
        __success = False
        __date_time = datetime.datetime.now()

        __cmd = "UPDATE %s "
        __cmd += "SET start_time='%s', is_active=%d "
        __cmd += "WHERE id=%d"
        __params = ( 'cluejhu_gameboard.tbl_game_time', __date_time, 0, 1 )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in setStartGameTime()" )
            __success = False
        else:
            self.log.msg.info( "query result was successful in setStartGameTime()" )
            __success = True

        return __success

    def resetCaseFileTbl( self ):
        """
        -----------------------------------------------------------------------
        Method:  resetCaseFileTbl( )::protected
        Purpose: updates the (tbl_case_file) table to clear any existing values
        Inputs:  None.
        Outputs: execution results: True/False
        -----------------------------------------------------------------------
        """
        __success = False
        self.log.msg.info( "preparing to reset database table: tbl_case_file" )

        __cmd = "UPDATE cluejhu_gameboard.tbl_case_file "
        __cmd += "SET card_1='%s', card_2='%s', card_3='%s' "
        __cmd += "WHERE id IS NOT %s";
        __params = ( 'NULL', 'NULL', 'NULL', 'NULL' )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetCaseFileTbl()" )
            __success = False
        else:
            self.log.msg.info( "query result was successful in resetCaseFileTbl()" )
            __success = True

        __success *= self.resetCaseFileTblSize()

        return bool( __success )

    def resetCaseFileTblSize( self ):
        """
        -----------------------------------------------------------------------
        Method:  resetCaseFileTblSize( )::protected
        Purpose: removes multiple rows from table if greater than 1
        Inputs:  None.
        Outputs: execution results: True/False
        -----------------------------------------------------------------------
        """
        __success = True
        __cmd = "SELECT %s FROM cluejhu_gameboard.tbl_case_file;"
        __params = ( 'id' )
        __result = ''

        try:
            __result = self.__query.SQLEecuteCmd( __cmd, ( __params, ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetCaseFileTblSize()" )
            __success *= False
        else:
            self.log.msg.info( "query result was successful in resetCaseFileTblSize()" )
            __success *= True

        if len( __result ) > 1:
            for __id in ( __result ):
                if not __id == __result[0]:
                    __cmd = 'DELETE FROM cluejhu_gameboard.tbl_case_file '
                    __cmd += 'WHERE %s = \'' + str( __id ) + '\'';
                    __params = ( 'id' )

                    try:
                        self.__query.SQLExecuteCmd( __cmd, ( __params ) )
                    except:
                        self.log.msg.info( 'SQL Command: ' + __cmd % __params )
                        self.log.msg.warn( "query result was NOT successful in resetCaseFileTblSize()" )
                        __success *= False
                    else:
                        self.log.msg.info( "query result was successful in resetCaseFileTblSize()" )
                        __success *= True

        return __success

    def resetHallTbl( self ):
        """
        -----------------------------------------------------------------------
        Method:  resetHallTbl( )::protected
        Purpose: updates the (tbl_hall) table to clear any existing values
        Inputs:  None.
        Outputs: execution results: True/False
        -----------------------------------------------------------------------
        """
        __success = False
        self.log.msg.info( "preparing to reset database table: tbl_hall" )

        __cmd = "UPDATE cluejhu_gameboard.tbl_hall "
        __cmd += "SET is_occupied='%d', occupant='%s' "
        __cmd += "WHERE id > %d;"
        __params = ( 0, 'NULL', 0 )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetHallTbl()" )
            __success = False
        else:
            self.log.msg.info( "query result was successful in resetHallTbl()" )
            __success = True
        return __success

    def resetNextMoveTbl( self ):
        """
        -----------------------------------------------------------------------
        Method:  resetNextMoveTbl( )::protected
        Purpose: updates the (tbl_next_move) table to clear any existing values
        Inputs:  None.
        Outputs: execution results: True/False
        -----------------------------------------------------------------------
        """
        __success = False
        self.log.msg.info( "preparing to reset database table: tbl_next_move" )
        __cmd = "UPDATE cluejhu_gameboard.tbl_next_move "
        __cmd += "SET pos_1='%s', pos_2='%s', pos_3='%s' "
        __cmd += "WHERE id > %d;"
        __params = ( 'NULL', 'NULL', 'NULL', 0 )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetNextMoveTbl()" )
            __success = False
        else:
            self.log.msg.info( "query result was successful in resetNextMoveTbl()" )
            __success = True

        return __success

    def resetNextTurnTbl( self ):
        """
        -----------------------------------------------------------------------
        Method:  resetNextTurnTbl( )::protected
        Purpose: updates the (tbl_next_turn) table to clear any existing values
        Inputs:  None.
        Outputs: execution results: True/False
        -----------------------------------------------------------------------
        """
        __success = False

        self.log.msg.info( "preparing to reset database table: tbl_next_turn" )

        __cmd = "UPDATE %s "
        __cmd += "SET name='%s' "
        __cmd += "WHERE id=%d"
        __params = ( 'cluejhu_gameboard.tbl_next_turn', 'clue_jhu', 1 )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetNextTurnTbl()" )
            __success = False
        else:
            self.log.msg.info( "query result was successful in resetNextTurnTbl()" )
            __success = True

        return __success

    def resetPlayerSuspectTbl( self ):
        """
        -----------------------------------------------------------------------
        Method:  resetPlayerSuspectTbl( )::protected
        Purpose: updates the (tbl_player_suspect) table to clear any existing values
        Inputs:  None.
        Outputs: execution results: True/False
        -----------------------------------------------------------------------
        """
        __success = False

        self.log.msg.info( "preparing to reset database table: tbl_player_suspect" )

        __cmd = "UPDATE cluejhu_gameboard.tbl_player_suspect "
        __cmd += "SET is_active=%d, is_suspect=%d "
        __cmd += "WHERE id > %d;"
        __params = ( 0, 0, 0, )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetPlayerSuspectTbl()" )
            __success = False
        else:
            self.log.msg.info( "query result was  successful in resetPlayerSuspectTbl()" )
            __success = True

        if __success == True:
            self.resetPlayerSuspectTbl_2()
        return __success

    def resetGamePlayLogs( self ):
        """
        -----------------------------------------------------------------------
        Method:  resetGamePlayLogs( )::protected
        Purpose: deletes previous game's log files (if exists)
        Inputs:  None.
        Outputs: execution results: True/False
        -----------------------------------------------------------------------
        """
        __success = False

        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.txt' ) )
            for __file in __files:
                os.remove( __file )

        if os.path.isdir( self.__dir ):
            __files = glob.glob( os.path.join( self.__dir, '*.txt' ) )

        if __files.__len__() == 0:
            __success = True
        else:
            self.log.msg.warning( 'unable to access game-play logs: ' + self.__dir )

        return __success

    def resetPlayerSuspectTbl_2( self ):
        __success = True

        __cmd = "UPDATE cluejhu_gameboard.tbl_player_suspect "
        __cmd += "SET name = '%s', curr_pos = '%s', last_pos= '%s' "
        __cmd += "WHERE id = %d;"
        __params = ( 'col_mustard', '1_3H2_3', 'NULL', 1 )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetPlayerSuspectTbl_2()" )
            __success = False
        else:
            self.log.msg.info( "query result was  successful in resetPlayerSuspectTbl_2()" )
            __success = True

        __cmd = "UPDATE cluejhu_gameboard.tbl_player_suspect "
        __cmd += "SET name = '%s', curr_pos = '%s', last_pos= '%s' "
        __cmd += "WHERE id = %d;"
        __params = ( 'dr_orchid', '3_2H3_3', 'NULL', 2 )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetPlayerSuspectTbl_2()" )
            __success = False
        else:
            self.log.msg.info( "query result was  successful in resetPlayerSuspectTbl_2()" )
            __success = True

        __cmd = "UPDATE cluejhu_gameboard.tbl_player_suspect "
        __cmd += "SET name = '%s', curr_pos = '%s', last_pos= '%s' "
        __cmd += "WHERE id = %d;"
        __params = ( 'ms_peacock', '2_1H3_1', 'NULL', 3 )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetPlayerSuspectTbl_2()" )
            __success = False
        else:
            self.log.msg.info( "query result was  successful in resetPlayerSuspectTbl_2()" )
            __success = True

        __cmd = "UPDATE cluejhu_gameboard.tbl_player_suspect "
        __cmd += "SET name = '%s', curr_pos = '%s', last_pos= '%s' "
        __cmd += "WHERE id = %d;"
        __params = ( 'ms_scarlett', '1_2H1_3', 'NULL', 4 )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetPlayerSuspectTbl_2()" )
            __success = False
        else:
            self.log.msg.info( "query result was  successful in resetPlayerSuspectTbl_2()" )
            __success = True

        __cmd = "UPDATE cluejhu_gameboard.tbl_player_suspect "
        __cmd += "SET name = '%s', curr_pos = '%s', last_pos= '%s' "
        __cmd += "WHERE id = %d;"
        __params = ( 'prof_plum', '1_1H2_1', 'NULL', 5 )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetPlayerSuspectTbl_2()" )
            self.log.msg.warn( "query result was NOT successful in resetPlayerSuspectTbl_2()" )
            __success = False
        else:
            self.log.msg.info( "query result was  successful in resetPlayerSuspectTbl_2()" )
            __success = True

        __cmd = "UPDATE cluejhu_gameboard.tbl_player_suspect "
        __cmd += "SET name = '%s', curr_pos = '%s', last_pos= '%s' "
        __cmd += "WHERE id = %d;"
        __params = ( 'rev_green', '3_1H3_2', 'NULL', 6 )

        try:
            self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        except:
            self.log.msg.info( 'SQL Command: ' + __cmd % __params )
            self.log.msg.warn( "query result was NOT successful in resetPlayerSuspectTbl_2()" )
            __success = False
        else:
            self.log.msg.info( "query result was  successful in resetPlayerSuspectTbl_2()" )
            __success = True

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------