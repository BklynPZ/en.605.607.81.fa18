#!/usr/bin/python2.7
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#
#   NAME:       Logging.py
#
#   PURPOSE:    This class provides logging facilities for all subsystem applications.
#
#   HISTORY:
#                CJ-123 | 2018.01.24 | Restructuring of internal system messages

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
import logging
import os
import sys
import inspect
import glob

#-------------------------------------------------------------------------------
class Logger:

    log_root = os.path.normpath( '/usr/local/source_code/cluejhu/GBM/logs/system' )
    __handle = None

    def __init__( self ):
        """
        -----------------------------------------------------------------------
        Method:  __init__( )::public
        Purpose: initial starting point of Process Registration script.
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        try:
            __class_name = str( sys._getframe( 1 ).f_locals['self'].__class__ ).split( '.' )[1]
        except KeyError:
            __class_name = None

        if __class_name is not None:
            self.msg = self.createLog( __class_name )

    def __del__( self, __caller = None ):
        """
        -----------------------------------------------------------------------
        Method:  __del__()::public
        Purpose: provides final garbage collection for objects before object destruction.
        Inputs:  None.
        Outputs: None.
        -----------------------------------------------------------------------
        """
        del self.msg

    def deleteLogs( self ):
        """
        -----------------------------------------------------------------------
        Method:  deleteLogs()::protected
        Purpose: deletes the system logs.
        Inputs:  None.
        Outputs: success result (True|False).
        -----------------------------------------------------------------------
        """
        __success = False

        if os.path.isdir( self.log_root ):
            __files = glob.glob( os.path.join( self.log_root, '*.log' ) )

            for __file in __files:
                os.unlink( __file )

            del __file

        if os.path.isdir( self.log_root ):
            __files = glob.glob( os.path.join( self.log_root , '*.log' ) )

        if __files.__len__() == 0:
            __success = True

        return __success

    def createLog( self, __name ):
        """
        -----------------------------------------------------------------------
        Method:  createLog( )::private
        Purpose: generates the initial log file for calling class and provides
                 formatting for the message passed.
        Inputs:  the name of to class object calling.
        Outputs: the object to the class's log file.
        ----------------------------------------------------------------------
        """
        __log_file = os.path.join( self.log_root, 'Module.' + __name + '.log' )

        prev_frame = inspect.currentframe().f_back
        __method = prev_frame.f_code.co_name

        logger = logging.getLogger( __name__ )
        logger.setLevel( logging.INFO )
        handler = logging.FileHandler( __log_file )
        handler.setLevel( logging.INFO )
        datefmt = '%Y.%m.%d_%I:%M:%S'
        formatter = logging.Formatter( '[' + '%(asctime)s] ' + '%(levelname)s' + ' :: ' + str( __method ) + '() :: ' + '%(message)s', datefmt )
        handler.setFormatter( formatter )
        logger.addHandler( handler )

        if not os.path.isfile( __log_file ):
            os.umask( 0 )
            with open( os.open( __log_file, os.O_CREAT | os.O_WRONLY, 0o777 ), 'w+' ) as self.__handle:
                __st = os.stat( __log_file )

        return logger

    def msg( self, __msg ):
        """
        -----------------------------------------------------------------------
        Method:  msg( )::public
        Purpose: provides input for calling class to log a message.
        Inputs:  message string from calling class.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        __message = __msg
        if not __message[__message.__len__() - 1:] == '.':
            __message += '.'

        self.msg( __message.strip() )

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------