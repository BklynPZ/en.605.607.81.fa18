#!/usr/bin/python
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#
#   NAME:      NextMoveCalculator.py
#
#   PURPOSE:   Provides mechanism to calculate players next possible move.
#
#   HISTORY:
#                CJ-125 | 2018.01.28 | Code clean-up based on Codacy Peer Review
#                CJ-123 | 2018.01.24 | Restructuring of internal system messages
#                CJ-072 | 2018.01.20 | Logging internal system messages
#                CJ-122 | 2019.01.18 | Code clean-up based on Codacy Peer Review
#                CJ-110 | 2018.11.08 | Code clean-up based on Codacy Peer Review
#                CJ-109 | 2018.11.03 | Import code to source control as baseline
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
import sys

#-------------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.GBM.logs.Logger import Logger
from cluejhu.SM.Gamer import Gamer
from cluejhu.GBM.Modules.dB_Interface import SQLInterface

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class NextMoveCalculator:

    def __init__( self ):
        """
        ------------------------------------------------------------------------
        Method:  __init__( )::public
        Purpose: provides initialization mechanism during object creation.
        Inputs:  None
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        self.log = Logger()
        self.__gamer = Gamer()
        self.__query = SQLInterface()

        # defines the rooms with adjacent halls and passage ways
        self.__rooms = {
                '1_1' : ['1_1H1_2', '1_1H2_1', '1_1P3_3'],
                '1_2' : ['1_1H1_2', '1_2H2_2', '1_2H1_2'],
                '1_3' : ['1_2H1_3', '1_3H2_3', '1_3P3_1'],
                '2_1' : ['1_1H2_1', '2_1H3_1', '2_1H2_2'],
                '2_2' : ['2_1H2_2', '1_2H2_2', '2_2H3_2', '2_2H2_3'],
                '2_3' : ['2_2H2_3', '1_3H2_3', '2_3H3_3'],
                '3_1' : ['2_1H3_1', '3_1H3_2', '3_1P1_3'],
                '3_2' : ['3_1H3_2', '2_2H3_2', '3_2H3_3'],
                '3_3' : ['3_2H3_3', '2_3H3_3', '1_1P3_3']
            }

        # defines the hallways connected to adjacent rooms
        self.__hallways = {
                '1_1H1_2' : ['1_1', '1_2'],
                '1_2H1_3' : ['1_2', '1_3'],
                '1_1H2_1' : ['1_1', '2_1'],
                '1_2H2_2' : ['1_2', '2_2'],
                '1_3H2_3' : ['1_3', '2_3'],
                '2_1H2_2' : ['2_1', '2_2'],
                '2_2H2_3' : ['2_2', '2_3'],
                '2_1H3_1' : ['2_1', '3_1'],
                '2_2H3_2' : ['2_2', '3_2'],
                '2_3H3_3' : ['2_3', '3_3'],
                '3_1H3_2' : ['3_1', '3_2'],
                '3_2H3_3' : ['3_2', '3_3']
            }

        self.__valid_moves = []
        self.__restrict = []

        self.log.msg.info( 'construction of object class object {{' + self.__class__.__name__ + '}} complete.' )

    # -------------------------------------------------------------------------

    def __del__( self ):
        """
        ------------------------------------------------------------------------
        Method:  __del__()::public
        Purpose: provides final garbage collection for objects before object destruction.
        Inputs:  None.
        Outputs: None.
        ------------------------------------------------------------------------
        """
        del self.__valid_moves[:]  # delete contents of list
        del self.__restrict[:]  # delete contents of list
        del self.__query  # sever connection to database
        self.log.msg.info( 'destruction of object class object {{' + self.__class__.__name__ + '}} complete.' )

    def __setMoveRestrictions( self, __name ):
        """
        ------------------------------------------------------------------------
        Method:  __setMoveRestrictions( )::private
        Purpose: retrieves location of all players and sets global variable to
                    restricts other players from going to that location.
        Inputs:  name of current player for exclusion rule.
        Outputs: None.
        ------------------------------------------------------------------------
        """

        self.log.msg.info( 'setting internal array to restrict users from selecting occupied location' )

        __cmd = "SELECT %s FROM %s "
        __cmd += "WHERE name !='%s' "
        __cmd += "AND is_active = %d;"
        __params = ( 'cur_pos', 'cluejhu_gameboard.tbl_player_suspect', __name, False )

        __occupied_pos = self.__query.SQLExecuteCmd( __cmd, ( __params ) )
        __occupied_pos = filter( None, __occupied_pos )  # removes any empty elements

        for __item in __occupied_pos:
            self.__restrict.append( __item )

    def __getPlayerLocation( self, __name ):
        """
        ------------------------------------------------------------------------
        Method:  __getPlayerLocation( )::private
        Purpose: queries database to retrieve current player location.
        Inputs:  None.
        Outputs: player location.
        ------------------------------------------------------------------------
        """

        __gamer = self.__gamer.convertToProperName( __name )

        self.log.msg.info( 'retrieving Gamer (' + __gamer + ')\'s current location' )

        __curr_pos = None
        __is_active = self.__gamer.getGamerStatus( __name )[0]

        if bool( __is_active ):
            __id = self.__gamer.getGamerIdByName( __name )
            __curr_pos = self.__gamer.getCurrentPos( __id )

        self.log.msg.info( 'Gamer (' + __gamer + ') is located here: ' + __curr_pos )

        return __curr_pos

    def __vetPossibleMoves( self, __moves ):
        """
        ------------------------------------------------------------------------
        Method:  __vetPossibleMoves( )::private
        Purpose: check projected moves against any restricted area - due to another
                    player occupying the location.
        Inputs:  None.
        Outputs: Nothing.
        ------------------------------------------------------------------------
        """
        for __item in __moves:
            if not __item in self.__restrict:
                self.__valid_moves.append( __item )

    def getPossibleMoves( self, __player_name ):
        """
        ------------------------------------------------------------------------
        Method:  getPossibleMoves( )::public
        Purpose: provides access point to get player's next possible moves.
        Inputs:  name of player.
        Outputs: possible player next moves.
        ------------------------------------------------------------------------
        """
        __curr_pos = self.__getPlayerLocation( __player_name )

        self.log.msg.info( 'Gamer\'s current position: ' + __curr_pos )

        if __curr_pos == None:
            del self.__valid_moves[:]  # delete contents of list
        else:
            self.__setMoveRestrictions( __player_name )

            if __curr_pos in self.__rooms.keys():
                __moves = self.__rooms[__curr_pos]
                self.__vetPossibleMoves( __moves )
            elif __curr_pos in self.__hallways.keys():
                __moves = self.__hallways[__curr_pos]

                self.__vetPossibleMoves( __moves )

        return self.__valid_moves

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------