#!/usr/bin/python2.7
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#
#   NAME:       dB_Interface.py
#
#   PURPOSE:    The SQL_Interface classes provides an interface to the back-end
#               MySQL database for gaming interactions.
#
#   HISTORY:
#               CJ-125 | 2018.01.28 | Code clean-up based on Codacy Peer Review
#               CJ-123 | 2018.01.24 | Restructuring of internal system messages
#               CJ-072 | 2018.01.20 | Logging internal system messages
#               CJ-122 | 2019.01.18 | Code clean-up based on Codacy Peer Review
#               CJ-120 | 2019.01.04 | Secure method of acquiring db login info
#               CJ-120 | 2019.01.04 | Fix functions for SQL Injection reformat
#               CJ-110 | 2018.11.08 | Code clean-up based on Codacy Peer Review
#               CJ-109 | 2018.11.03 | Import code to source control as baseline
#
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
import MySQLdb
import os
import sys
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

class SQLConnector( object ):

    def __init__( self ):
        """
        -----------------------------------------------------------------------
        Method:  <SQLConnector>__init__( )::protected
        Purpose: provides initialization mechanism for super/generalization
                 object instantiation and allows object to connect to MySQL 
                 database used by the system.
        Inputs:  None
        Outputs: Nothing
        -----------------------------------------------------------------------
        """

        __filename = ".db_login"
        __file = os.path.join( os.path.dirname( os.path.realpath( __file__ ) ), __filename )

        self.__server = "localhost"
        self.__schema = "cluejhu_gameboard"

        if not os.path.isfile( __file ):
            pass
        else:
            with open( __file ) as __f:
                __content = __f.readlines()

            for __line in __content:
                if "username" in __line:
                    self.__user = __line.split( ":" )[1].rstrip()
                if "password" in __line:
                    self.__passwd = __line.split( ":" )[1].rstrip()

        self.establishConnection()

    # ----------------------------------------------------------------------

    def establishConnection( self ):
        """
        -----------------------------------------------------------------------
        Method:  establishConnection( )::public
        Purpose: calls to Super Class to execute function to reconnect to the 
                 MySQL database.
        Inputs:  None.
        Outputs: result from successful or failed connection TRUE/FALSE.
        -----------------------------------------------------------------------
        """
        __db = MySQLdb.connect( self.__server,
                                    self.__user,
                                    self.__passwd,
                                    self.__schema )
        __db.autocommit( True )
        self.__cursor = __db.cursor()  # pointer to cursor object

    # ----------------------------------------------------------------------

    def testConnection( self ):
        """
        -----------------------------------------------------------------------
        Method:  <SQLConnector>testConnection( )::protected
        Purpose: tests the Super/Generalization Class' connection to MySQL.
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        __success = False

        try:
            self.__cursor.execute( "SELECT VERSION()" )
            __results = self.__cursor.fetchone()

            if len( __results ) == 1:
                __success = True
        except MySQLdb.Error:
            return __success
        else:
            return __success

    # ----------------------------------------------------------------------

    def restablishConnection( self ):
        """
        -----------------------------------------------------------------------
        Method:  establishConnection( )::public
        Purpose: calls to Super Class to execute function to reconnect to the 
                 MySQL database.
        Inputs:  None.                pass
        Outputs: result from successful or failed connection TRUE/FALSE.
        -----------------------------------------------------------------------
        """
        self.establishConnection()

    # ----------------------------------------------------------------------

    def SQLExecuteCmd( self, __query, __params ):
        """
        -----------------------------------------------------------------------
        Method:  <SQLConnector>SQLExecuteCmd( )::protected
        Purpose: executes the SQL query command passed to this method.
        Inputs:  SQL query string and a list of values.
        Outputs: returns a list of the result.
        -----------------------------------------------------------------------
        """
        __result = []
        __cmd = __query % __params

        try:
            if not self.testConnection():
                self.__init__()

        except Exception as __err:
            import traceback
            traceback.print_exc( __err )
        else:
            __result = self.__cursor.execute( __cmd )

            if "SELECT" in __cmd:
                try:
                    __rows = self.__cursor.fetchall()
                except:
                    pass
                else:
                    if __rows.__len__() == 0:
                        __result = [0]
                    elif __rows.__len__() == 1:
                        __row = [ __item[0] for __item in __rows ]
                        __result = __row
                    else:
                        __result = [ __item[0] for __item in __rows ]

        __query = None
        __params = None

        return __result

    # ----------------------------------------------------------------------

    def __del__( self ):
        """
        -----------------------------------------------------------------------
        Method:  __del__()::public
        Purpose: provides final garbage collection for objects before object destruction.
        Inputs:  None.
        Outputs: None.
        -----------------------------------------------------------------------
        """
        if self.__cursor is None:
            self.__cursor

#---------------------------------------------------------------------------
#---------------------------------------------------------------------------

class SQLInterface( SQLConnector ):

    def __init__( self ):
        """
        -----------------------------------------------------------------------
        Method:  __init__( )::public
        Purpose: provides initialization mechanism for object creation.
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        try:
            self.__caller = str( sys._getframe( 1 ).f_locals['self'].__class__ ).split( '.' )[1]
        except:
            self.__caller = self.__class__.__name__

        self.__results = ""
        super( SQLInterface, self ).__init__()

    # ----------------------------------------------------------------------

    def testConnection( self ):
        """
        -----------------------------------------------------------------------
        Method:  testConnection( )::public
        Purpose: calls to Super Class to execute function to test the database 
                    connection to MySQL.
        Inputs:  None.
        Outputs: result from successful or failed connection TRUE/FALSE.
        -----------------------------------------------------------------------
        """
        return super( SQLInterface, self ).testConnection()

    # ----------------------------------------------------------------------

    def restablishConnection( self ):
        """
        -----------------------------------------------------------------------
        Method:  restablishConnection( )::public
        Purpose: calls to Super Class to execute function to reconnect to the 
                 MySQL database.
        Inputs:  None.
        Outputs: result from successful or failed connection TRUE/FALSE.
        -----------------------------------------------------------------------
        """
        super( SQLInterface, self ).establishConnection()

    # ----------------------------------------------------------------------

    def SQLExecuteCmd( self, __cmd, __params ):
        """
        -----------------------------------------------------------------------
        Method:  SQLCommand( )::public
        Purpose: provides access to execute a free-form MySQL query.
        Inputs:  None.
        Outputs: returns result from query.
        -----------------------------------------------------------------------
        """
        self.__result = super( SQLInterface, self ).SQLExecuteCmd( __cmd, __params )

        return self.__result

    # ----------------------------------------------------------------------

    def __del__( self ):
        """
        -----------------------------------------------------------------------
        Method:  __del__( )::public
        Purpose: provides final garbage collection for objects before object destruction.
        Inputs:  None.
        Outputs: Nothing.
        -----------------------------------------------------------------------
        """
        super( SQLInterface, self ).__del__()

#---------------------------------------------------------------------------
#---------------------------------------------------------------------------

# class X:
#     def __init__(self):
#         x = SQLInterface()
#         del x
#
# x = X()
# del x