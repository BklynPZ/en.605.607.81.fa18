#!/usr/bin/python
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#
#   NAME:      MultiCastLogger.py
#
#   PURPOSE:   Provides mechanism print messages to player's console by saving
#              the output of a system message to a log file read by the console.
#
#   HISTORY:
#                CJ-123 | 2018.01.24 | Restructuring of internal system messages
#                CJ-077 | 2019.01.05 | Standard message received at user console
#                CJ-081 | 2018.01.20 | Logging standardized messages
#                CJ-072 | 2018.01.20 | Logging internal system messages
#                CJ-108 | 2018.12.31 | Provide player access after authentication
#                CJ-110 | 2018.11.08 | Code clean-up based on Codacy Peer Review
#                CJ-109 | 2018.11.03 | Import code to source control as baseline
#
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
import os
import time
import datetime
import sys
import socket

#-------------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.SM.Gamer import Gamer
from cluejhu.GBM.logs.Logger import Logger
from cluejhu.GBM.Modules.dB_Interface import SQLInterface

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class MultiCastLogger:

    def __init__( self ):
        """
        ------------------------------------------------------------------------
        Method:  __init__( )::public
        Purpose: provides initialization mechanism during object creation.
        Inputs:  None
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        self.log = Logger()
        self.__gamer = Gamer()
        self.__query = SQLInterface()
        self.__handle = None

        __this_script = os.path.realpath( __file__ )
        __parent_dir = os.path.dirname( os.path.dirname( __file__ ) )
        __filename = os.path.join( __parent_dir, "logs", "gameplay", "multicast-messages.txt" )

        if not os.path.isfile( __filename ):
            os.system( 'touch ' + __filename )
            os.chmod( __filename, 0o776 )

        self.__filename = __filename

        self.log.msg.info( 'construction of object class object {{' + self.__class__.__name__ + '}} complete.' )

    def __del__( self ):
        """
        -----------------------------------------------------------------------
        Method:  __del__()::public
        Purpose: provides final garbage collection for objects before object destruction.
        Inputs:  None.
        Outputs: None.
        -----------------------------------------------------------------------
        """
        self.__handle.close()
        # del self.__query
        # self.log.msg.info( 'destruction of object class object {{' + self.__class__.__name__ + '}} complete.' )

    def getIpAddress( self ):
        """
        -----------------------------------------------------------------------
        Method:  getIpAddress()::private
        Purpose: determine the IP address of the gamer and return it to the caller method
        Inputs:  None.
        Outputs: None.
        -----------------------------------------------------------------------
        """
        __s = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        try:
            # doesn't even have to be reachable
            __s.connect( ( '10.255.255.255', 1 ) )
            __IP = __s.getsockname()[0]
        except:
            __IP = '127.0.0.1'
        finally:
            __s.close()

        return __IP

    def __write2Logger( self, __message ):
        """
        ------------------------------------------------------------------------
        Method:  __write2Logger( )::private
        Purpose: writes input messages to multi-case log file.
        Inputs:  message to write
        Outputs: Nothing
        ------------------------------------------------------------------------
        """

        with open( self.__filename, 'a' ) as self.__handle:
            self.__handle.write( __message + "\n" )

        del __message

    def __formatLocation( self, __location ):
        """
        ------------------------------------------------------------------------
        Method:  __formatLocation( )::private
        Purpose: .
        Inputs:  None
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        if "H" in __location:
            __cmd = "SELECT name FROM cluejhu_gameboard.tbl_hall WHERE coords = '%s';"

        elif "P" in __location:
            __cmd = "SELECT rid FROM cluejhu_gameboard.tbl_passage WHERE coords = '%s';"
            __id = self.__query.SQLExecuteCmd( __cmd, ( __location, ) )
            __location = __id[0].__str__()  #---------------------------------------------------------------------------
            __cmd = "SELECT name FROM cluejhu_gameboard.tbl_rooms WHERE id = '%s';"

        else:
            __cmd = "SELECT name FROM cluejhu_gameboard.tbl_rooms WHERE coords = '%s';"

        __result = self.__query.SQLExecuteCmd( __cmd, ( __location, ) )
        __location = __result[0].title()

        return __location

    def __getTime( self ):
        """
        ------------------------------------------------------------------------
        Method:  __getTime( )::private
        Purpose: .
        Inputs:  None
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        __ts = time.time()
        __timestamp = datetime.datetime.fromtimestamp( __ts ).strftime( '%H:%M:%S' )

        return __timestamp

    def logPlayerMove( self, __player, __location ):
        """
        ------------------------------------------------------------------------
        Method:  logPlayerMove( )::public
        Purpose: .
        Inputs:  None
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        __msg = ""
        __msg += "[" + self.__getTime() + "]: "

        __player = self.__gamer.convertToProperName( __player )
        __location = self.__formatLocation( __location )

        if "_" in __location:
            __location = __location.replace( '_', ' ' )

        __msg += __player + " has moved to the \"" + __location + "\"."

        self.__write2Logger( __msg )

    def logSuggestion( self, __player, __suspect, __weapon, __room ):
        """
        ------------------------------------------------------------------------
        Method:  logSuggestion( )::public
        Purpose: logs player suggestions.
        Inputs:  player name, suspect name, a weapon and a room
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        __timestamp = "[" + self.__getTime() + "]: "
        __msg = __timestamp

        __player = self.__gamer.convertToProperName( __player )
        __suspect = self.__gamer.convertToProperName( __suspect )

        __msg += __timestamp + __player + " suggests " + __suspect + " committed a crime.\n"
        __msg += __timestamp + __player + " suggests a \"" + __weapon.title() + "\" was used.\n"
        __msg += __timestamp + __player + " suggests the crime occurred in the \"" + __room.title() + "\".\n"

        __msg += "-----------------------------------------------------------"

        self.__write2Logger( __msg )

    def logAccusation( self, __player, __suspect, __weapon, __room ):
        """
        ------------------------------------------------------------------------
        Method:  logAccusation( )::public
        Purpose: logs player accusations.
        Inputs:  player name, suspect name, a weapon and a room
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        __timestamp = "[" + self.__getTime() + "]: "
        __msg = __timestamp

        __player = self.__gamer.convertToProperName( __player )
        __suspect = self.__gamer.convertToProperName( __suspect )

        __msg += __player + " believes a \"" + __weapon.title() + "\" was used.\n"
        __msg += __timestamp + __player + " believes in the \"" + __room.title() + "\".\n"
        __msg += __timestamp + __player + " now accuses " + __suspect + "!!!\n"
        __msg += "-----------------------------------------------------------"

        self.__write2Logger( __msg )

    def logGameWinner( self, __player ):
        """
        ------------------------------------------------------------------------
        Method:  logGameWinner( )::public
        Purpose: logs the game winner.
        Inputs:  None
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        __player = self.__gamer.convertToProperName( __player )

        __msg = "************************************************************\n"
        __msg += "****    " + __player.upper() + " ... won the game!!! ****\n"
        __msg += "***********************************************************\n"

        self.__write2Logger( __msg )

    def logNewPlayer( self, __player ):
        """
        ------------------------------------------------------------------------
        Method:  logNewPlayer( )::public
        Purpose: logs entry of a new player to the game.
        Inputs:  None
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        __player = self.__gamer.convertToProperName( __player )

        __ip_address = self.getIpAddress()
        __timestamp = "[" + self.__getTime() + "]: "
        __msg = __timestamp
        __msg += "**** " + __player + " @(" + __ip_address + ") joined the game.****"

        self.__write2Logger( __msg )

    def logStatus( self, __player, __status = True, __reason = "" ):
        """
        ------------------------------------------------------------------------
        Method:  logStatus( )::public
        Purpose: logs the current status of the gamer.
        Inputs:  None
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        self.__gamer.setGamerStatus( __player, __status )

        __player = self.__gamer.convertToProperName( __player )
        __timestamp = "[" + self.__getTime() + "]: "
        __msg = __timestamp

        if __status:
            __msg += __player + "\'s status is now \"ACTIVE\""
        else:
            __msg += __player + "\'s status is now \"INACTIVE\""
            if not len( __reason ) == 0:
                __msg += " due to " + __reason

        self.__write2Logger( __msg )

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
