#!/usr/bin/python2.7
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#
#   NAME:       ProcessRegistration.py
#
#   PURPOSE:    Processes registration data from registration form.
#
#   HISTORY:
# 				CJ-123 | 2018.01.24 | Restructuring of internal system messages
# 				CJ-122 | 2019.01.18 | Code clean-up based on Codacy Peer Review
# 				CJ-071 | 2019.01.05 | Logging user interactions
# 				CJ-072 | 2019.01.05 | Logging internal system messages
# 				CJ-108 | 2018.12.31 | Provide player access after authentication
# 				CJ-076 | 2018.12.30 | Provide set of characters for player selection
# 				CJ-110 | 2018.11.08 | Code clean-up based on Codacy Peer Review
# 				CJ-109 | 2018.11.03 | Import code to source control as baseline
#
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
import cgi
import cgitb
import sys

#-------------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.GBM.Game import Game
from cluejhu.SM.Gamer import Gamer
from cluejhu.GBM.logs.Logger import Logger
from cluejhu.GBM.Modules.MultiCastLogger import MultiCastLogger

#-------------------------------------------------------------------------------
cgitb.enable()

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class ProcessRegistration:

	def __init__( self ):
		"""
		-----------------------------------------------------------------------
		Method:  __init__( )::public
		Purpose: provides initialization mechanism for object creation.
		Inputs:  None
		Outputs: Nothing.
		-----------------------------------------------------------------------
		"""
		self.__html_code = ""

		self.log = Logger()
		self.__gamer = Gamer()
		self.__game = Game()
		self.__msgboard = MultiCastLogger()

		self.log.msg.info( 'construction of object class object {{' + self.__class__.__name__ + '}} complete.' )

	def __del__( self ):
		"""
		-----------------------------------------------------------------------
		Method:  __del__( )::public
		Purpose: object destruction of class.
		Inputs:  None
		Outputs: Nothing.
		-----------------------------------------------------------------------
		"""
		self.log.msg.info( 'destruction of object class object {{' + self.__class__.__name__ + '}} complete.' )

	def getHtmlPageResults( self ):
		"""
		--------------------------------------------------------------------
		Method:  getHtmlPageResults( )::public
		Purpose: returns the HTML content for redirected Registration page
		Inputs:  None.
		Outputs: HTML code in string value
		--------------------------------------------------------------------"""
		return self.__html_code

	def getFormData( self ):
		"""
		--------------------------------------------------------------------
		Method:  getFormData( )::private
		Purpose: collects the data from the web browser )
		Inputs:  None.
		Outputs: name of user-selected character.
		--------------------------------------------------------------------"""
		try:
			__form = cgi.FieldStorage()
		except:
			__char_name = None
		else:
			__char_name = __form.getvalue( 'name' )
			__char_name = __form['player'].value

		__charname = self.__gamer.convertToProperName( __char_name )
		self.log.msg.info( 'new player has requested: ' + str( __charname ) + ' for this gaming session.' )

		return __char_name

	def processPlayerAssignment( self, __char_name ):
		"""
		--------------------------------------------------------------------
		Method:  processPlayerAssignment( )::private
		Purpose: processes player request for a particular character
		Inputs:  None.
		Outputs:		#self.log.info( 'object construction complete.")
		--------------------------------------------------------------------"""
		__html_text = ""
		__max_count = 6

		__num_gamers = self.__gamer.getActiveGamers()

		if __num_gamers == 0:
			self.__game.restartGame()

		__is_active = bool( self.__gamer.getGamerStatus( __char_name )[0] )

		if __is_active == True:

			self.log.msg.warn( 'requested character has already been registered.' )
			self.log.msg.info( 'redirecting gamer to failed registration page.' )

			__proper_char_name = self.__gamer.convertToProperName( __char_name )
			__proper_char_name = "xxx"
			__redirectURL = "../index.html"

			__html_text = 'Content-type: text/html\n\n'
			__html_text += '<html>'
			__html_text += '  <head>'
			__html_text += '    <title>ClueJHU URL Redirect</title>'
			__html_text += '  </head>'
			__html_text += '  <body>'
			__html_text += '      <h2><br>We\'re Sorry ...</h2>'
			__html_text += '      <h2><font color="darkred">' + __proper_char_name + '\'s</font> character is already taken!</h2>'
			__html_text += '      <h3>Please make another selection ...</h3>'
			__html_text += '      <br><br>'
			__html_text += '      <br><br>Redirecting... <a href="%s">Click here if you are not redirected</a>' % __redirectURL
			__html_text += '  </body>'
			__html_text += '</html>'

		elif __is_active == False:

			self.log.msg.info( 'new player has been registered: ' + str( __char_name ) )
			self.log.msg.info( 'redirecting registered Gamer to home page.' )

			self.__gamer.setGamerStatus( __char_name, True )
			self.__gamer.setSuspectStatus( __char_name, True )

			self.__msgboard.logNewPlayer( __char_name )
			self.__msgboard.logStatus( __char_name )

			__redirectURL = "../console.html?user=" + __char_name

			__html_text = 'Content-type: text/html\n\n'
			__html_text += 'Location: %s' % __redirectURL
			__html_text += '<html>'
			__html_text += '  <head>'
			__html_text += '    <meta http-equiv="refresh" content="3;url=%s" />' % __redirectURL
			__html_text += '    <title>You are going to be redirected</title>'
			__html_text += '  </head>'
			__html_text += '  <body>'
			__html_text += '      <h2><br>Congratulations!!! ... </h2>'
			__html_text += '      <h2>Your Character Name is: <font color="darkgreen">' + __char_name + '</font></h2>'
			__html_text += '      <br><br>Redirecting... <a href="%s">Click here if you are not redirected</a>' % __redirectURL
			__html_text += '  </body>'
			__html_text += '</html>'

			self.log.msg.info( 'current registered Gamers are : ' + str( __num_gamers ) )

		elif __num_gamers == __max_count:

			self.log.msg.warn( 'number of registered Gamers (' + str( __num_gamers ) + ') at game\'s maximum allowance.' )
			self.log.msg.info( 'redirecting unregistered Gamer to home page.' )

			__redirectURL = "../index.html"

			__html_text = 'Content-type: text/html\n\n'
			__html_text += '<html>'
			__html_text += '  <head>'
			__html_text += '    <title>ClueJHU URL Redirect</title>'
			__html_text += '    <meta http-equiv="refresh" content="3;url=%s" />' % __redirectURL
			__html_text += '  </head>'
			__html_text += '  <body>'
			__html_text += '      <h2><br>We\'re Sorry ...</h2>'
			__html_text += '      <h2><font color="darkred"> Maximum number of Gamer\'s have been reached</h2>'
			__html_text += '      <h3>Please try again in a few minutes ...</h3>'
			__html_text += '      <br><br>'
			__html_text += '		 <button onclick="window.location.href=\'' + __redirectURL + '\'">Continue to Registration</button>'
			__html_text += '  </body>'
			__html_text += '</html>'

			self.log.msg.info( 'current registered gamer\'s are : ' + __num_gamers.__str__() )

		self.__html_code = __html_text

#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
if __name__ == "__main__":

	__web_form = ProcessRegistration()
	__char_name = __web_form.getFormData()
	__web_form.processPlayerAssignment( __char_name.__str__() )
	__html = __web_form.getHtmlPageResults()

	print ( __html )

#---------------------------------------------------------------------------
#---------------------------------------------------------------------------