function readMultiCastFile(file)
{
	var rawFile = new XMLHttpRequest();

	rawFile.open("GET", file, false);
	rawFile.onreadystatechange = function ()
	{
		if(rawFile.readyState === 4)
		{
			if(rawFile.status === 200 || rawFile.status == 0)
			{
				var allText = rawFile.responseText;

				// set updates message console to player
				$('textarea#player_messages').val(allText);
			}
		}
	}
	rawFile.send(null);
}

$(document).ready(function(){

	var path = "../../GBM/logs/gameplay/multicast-messages.txt";	
	var timeout = setInterval(readMultiCastFile, 5000);

	<script>alert(path);</script>


	readMultiCastFile(path);

	$("#move_submit_button, #suggest_submit_button, #accuse_submit_button").hide();

	$("#move_button").on("click",function(){
		$(this).hide();
		$("#frmSuggest, #frmAccuse").hide()
		$("#move_submit_button, #suggest_submit_button, #accuse_submit_button").hide();   
		$("#frmMove, #move_submit_button").show();   
	});

	$("#suggest_button").on("click",function(){
		$(this).hide();
		$("#frmAccuse, #frmMove").hide()
		$("#move_submit_button, #suggest_submit_button, #accuse_submit_button").hide();   
		$("#frmSuggest, #suggest_submit_button").show();   
	});

	$("#accuse_button").on("click",function(){
		$(this).hide();
		$("#frmSuggest, #frmMove").hide()
		$("#move_submit_button, #suggest_submit_button, #accuse_submit_button").hide();   
		$("#frmAccuse, #accuse_submit_button").show();   
	});

	// Displays NextMove to the Player

	$("input:radio[name=move]").show();

	var arrNextMoves,coords,room_name,data,i;

	arrNextmoves = '{"2_3": "Ballroom", "2_3H3_3": "South Ballroom Hallway", "2_2H2_3": "East Conservatory Room"}';    
	data = JSON.parse(arrNextmoves);
	i = -1;

	for ( var coords in data) {
		i += 1;
		room_name = data[coords];
		$('input[name="move"]')[i].value = coords;
		document.getElementById('frmMove').getElementsByTagName('label')[i].innerHTML = " " + room_name;
	}
});