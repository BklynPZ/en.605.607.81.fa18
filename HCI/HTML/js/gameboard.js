var timeout = setInterval(reloadGameBoard, 5000);

function reloadGameBoard(file, callback) {

	var rawFile = new XMLHttpRequest();

	rawFile.overrideMimeType("application/json");
	rawFile.open("GET", file, true);

	rawFile.onreadystatechange = function() {
		if (rawFile.readyState === 4 && rawFile.status == "200") {
			callback(rawFile.responseText);
		}
	}
	rawFile.send(null);
}

$(document).ready(
		function() {

			var key;
			var path = "logs/gameboard_status.txt";

			reloadGameBoard(path, function(text) {

				var data = JSON.parse(text);

				$("img").attr("src", "/icons/blank.png");

				for ( var key in data) {
					console.log(key);
					console.log(data[key]);
					$("#" + key).children("img").attr("src",
							"/icons/" + data[key] + ".png").delay(300);
				}

			});
		});