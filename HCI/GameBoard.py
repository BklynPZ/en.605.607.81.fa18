#!/usr/bin/python
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#
#   NAME:        Gameboard.py
#
#   PURPOSE:     The Gameboard is the heart of the program providing internal access
#                to features that make the game work.
#
#   HISTORY:
#                CJ-125 | 2018.01.28 | Code clean-up based on Codacy Peer Review
#                CJ-123 | 2018.01.24 | Restructuring of internal system messages
#                CJ-072 | 2018.01.20 | Logging internal system messages
#                CJ-110 | 2018.11.08 | Code clean-up based on Codacy Peer Review
#                CJ-109 | 2018.11.03 | Import code to source control as baseline
#
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
import itertools
import stat
import os
import json
import sys
#-------------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.GBM.logs.Logger import Logger
from cluejhu.GBM.Modules.dB_Interface import SQLInterface

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class Gameboard:

    def __init__( self ):
        """
        ------------------------------------------------------------------------
        Method:  __init__( )::public
        Purpose: provides initialization mechanism during object creation.
        Inputs:  None
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        self.log = Logger()
        self.query = SQLInterface()

        __this_script = os.path.realpath( __file__ )
        __parent_dir = os.path.dirname( os.path.dirname( __file__ ) )
        self.__filename = os.path.join( __parent_dir, "GBM", "logs", "gameplay", "gameboard_status.txt" )

        if not os.path.isfile( self.__filename ):
            with open( self.__filename, "w" ) as __handle:
                __handle.close()
                __st = os.stat( self.__filename )
                os.chmod( self.__filename, __st.st_mode | stat.S_IEXEC )

        self.log.msg.info( 'construction of object class object {{' + self.__class__.__name__ + '}} complete.' )
    # -------------------------------------------------------------------------

    def __del__( self ):
        """
        --------------------------------------------------------------------
        Method:  __del__()::public
        Purpose: provides final garbage collection for objects before object destruction.
        Inputs:  None.
        Outputs: None.
        --------------------------------------------------------------------
        """
        # print "code goes here ..."
        self.log.info( 'object disposal complete.' )

    # -------------------------------------------------------------------------

    def setBoardGameLocations( self ):

        __datastore = self.__getPlayerPositions()

        with open( self.__filename, 'w' ) as __handle:
            json.dump( __datastore, __handle )

        __handle.close()

    # -------------------------------------------------------------------------

    def __getPlayerPositions( self ):

        __names = []
        __cur_pos = []

        __dict = {}

        __cmd = "SELECT cur_pos FROM cluejhu_gameboard.tbl_player_suspect WHERE is_active = %s;"
        __positions = self.query.SQLExecuteCmd( __cmd, ( 1, ) )

        for __pos in __positions:
            __cmd = "SELECT name FROM cluejhu_gameboard.tbl_player_suspect WHERE cur_pos = '%s';"
            __name = self.query.SQLExecuteCmd( __cmd, ( __pos.__str__(), ) )[0]

            __cur_pos.append( __pos.__str__() )
            __names.append( __name.__str__() )

        __dict = dict( itertools.izip( __cur_pos, __names ) )

        return __dict

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
