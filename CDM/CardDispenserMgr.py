#!/usr/bin/python
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#
#   NAME:       CardDispenserMgr.py
#
#   PURPOSE:    The CardDispenserManager class manages the shuffling of game-cards
#               and maintains the confidential "case file" which reveals who is
#               the perpetrator of the crime against Mr Boody.
#
#   HISTORY:
#                CJ-123 | 2018.01.24 | Restructuring of internal system messages
#                CJ-122 | 2019.01.18 | Code clean-up based on Codacy Peer Review
#                CJ-071 | 2018.01.06 | Logging user interactions
#                CJ-072 | 2018.01.06 | Logging internal system messages
#                CJ-110 | 2018.11.08 | Code clean-up based on Codacy Peer Review
#                CJ-109 | 2018.11.03 | Import code to source control as baseline
#
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
import sys

#-------------------------------------------------------------------------------
sys.path.append( "/usr/local/source_code" )

from cluejhu.GBM.logs.Logger import Logger
from cluejhu.GBM.Modules.dB_Interface import SQLInterface
from random import shuffle

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class CardDispenserManager:

    def __init__( self ):
        """
        ------------------------------------------------------------------------
        Method:  __init__( )::public
        Purpose: provides initialization mechanism during object creation:
                 - Cards: Suspect, Weapons, Rooms
                 - Case Fileimport os
import glob
import time
        Inputs:  None
        Outputs: Nothing
        ------------------------------------------------------------------------
        """
        self.log = Logger()
        self.__query = SQLInterface()

        self.__suspect_cards = list()
        self.__weapons_cards = list()
        self.__room_cards = list()

        self.__case_file = list()

        self.populateCardLists()
        self.shuffleCards()
        self.setCaseFile()

        self.log.msg.info( 'construction of object class object {{' + self.__class__.__name__ + '}} complete.' )

    def __del__( self ):
        """
        --------------------------------------------------------------------
        Method:  __del__()::public
        Purpose: provides final garbage collection for objects before object destruction.
        Inputs:  None.
        Outputs: None.
        --------------------------------------------------------------------
        """
        self.__query.__del__()
        self.log.msg.info( 'destruction of object class object {{' + self.__class__.__name__ + '}} complete.' )

    def resetCardDecks( self ):
        """
        --------------------------------------------------------------------
        Method:  resetCardDecks()::public
        Purpose: deletes all lists holding card deck elements
        Inputs:  None.
        Outputs: None.
        --------------------------------------------------------------------
        """
        self.log.msg.info( 'resetting database table list values to NULL for: cluejhu_gameboard.tbl_case_file.' )

        __cmd = "DELETE FROM %s WHERE NOT %s = %d"
        __params = ( 'cluejhu_gameboard.tbl_case_file', 'id', 0 )
        self.__query.SQLExecuteCmd( __cmd, __params )

        self.log.msg.info( 'resetting auto increment value to 1 for: cluejhu_gameboard.tbl_case_file.' )

        __cmd = 'ALTER TABLE %s AUTO_INCREMENT = %d';
        __params = ( 'cluejhu_gameboard.tbl_case_file', 1 )
        self.__query.SQLExecuteCmd( __cmd, __params )

    def populateCardLists( self ):
        """
        --------------------------------------------------------------------
        Method:  populateCardLists()::private
        Purpose: populates list of card items for CLUE game.
        Inputs:  None.
        Outputs: Nothing.
        --------------------------------------------------------------------
        """
        self.log.msg.info( 'populating the card deck with values.' )

        self.__suspect_cards.append( "col_mustard" )
        self.__suspect_cards.append( "dr_orchid" )
        self.__suspect_cards.append( "ms_peacock" )
        self.__suspect_cards.append( "ms_scarlett" )
        self.__suspect_cards.append( "prof_plum" )
        self.__suspect_cards.append( "rev_green" )

        self.__weapons_cards.append( "candle_holder" )
        self.__weapons_cards.append( "dagger" )
        self.__weapons_cards.append( "pipe" )
        self.__weapons_cards.append( "revolver" )
        self.__weapons_cards.append( "rope" )
        self.__weapons_cards.append( "wrench" )

        self.__room_cards.append( "study" )
        self.__room_cards.append( "hall" )
        self.__room_cards.append( "lounge" )
        self.__room_cards.append( "library" )
        self.__room_cards.append( "billards" )
        self.__room_cards.append( "dining_room" )
        self.__room_cards.append( "conservatory" )
        self.__room_cards.append( "ballroom" )
        self.__room_cards.append( "kitchen" )

    def shuffleCards( self ):
        """
        --------------------------------------------------------------------
        Method:  shuffleCards()::private
        Purpose: provides shuffling functionality to list.
        Inputs:  None.
        Outputs: Nothing.
        --------------------------------------------------------------------
        """
        self.log.msg.info( 'executing functionality to shuffle list of ' )

        shuffle( self.__suspect_cards )
        shuffle( self.__weapons_cards )
        shuffle( self.__room_cards )

    def setCaseFile( self ):
        """
        --------------------------------------------------------------------
        Method:  setCaseFile()::private
        Purpose: sets the Case File variable for the CLUE game.
        Inputs:  None.
        Outputs: Nothing.
        --------------------------------------------------------------------
        """
        __x = self.__suspect_cards.pop()
        __y = self.__weapons_cards.pop()
        __z = self.__room_cards.pop()

        __cmd = "INSERT INTO cluejhu_gameboard.tbl_case_file (card_1,card_2,card_3) "
        __cmd += "VALUES ('%s','%s','%s');"
        __params = ( __x, __y, __z )

        self.__query.SQLExecuteCmd( __cmd, __params )

        self.log.msg.info( 'updating card deck to database.' )

    def getCaseFile( self ):
        """
        --------------------------------------------------------------------
        Method:  getCaseFile()::public
        Purpose: provides garbage collection for code.
        Inputs:  None.
        Outputs: list of a randomly selected suspect, weapon and room.
        --------------------------------------------------------------------
        """
        self.log.msg.info( 'returning case file to caller method' )

        return self.__case_file

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------